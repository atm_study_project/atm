#include <iostream>
using namespace std;

#include <mysql.h>
#include <list>

#include "Dao.h"
#include "Client.h"

#define STRING_SIZE 50

#define DROP_SAMPLE_TABLE "DROP TABLE IF EXISTS test_table"
#define CREATE_SAMPLE_TABLE "CREATE TABLE test_table(col1 INT,\
                                                 col2 VARCHAR(40),\
                                                 col3 SMALLINT,\
                                                 col4 TIMESTAMP)"
#define INSERT_SAMPLE "INSERT INTO \
                       test_table(col1,col2,col3) \
                       VALUES(?,?,?)"

struct A{
	A(){}
	virtual ~A(){}
};
struct B:public virtual A
{
	B():A(){}
	virtual ~B(){}
};

void f(A* a)
{
	//B *b = a;
}

int main(void)
{
	A *a;
	B b;
	a = &b;
	MYSQL *conn(0);
	char *server = "127.0.0.1";
    char *user = "root";
    char *password = "45xf4dl7";
    char *database = "test";
	conn = mysql_init(NULL);
   
   if (!mysql_real_connect(conn, server,
         user, password, database, 3306, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return 0; 
   }

	shared_ptr<MYSQL> conn_ptr(conn);

	DaoClient dc(conn_ptr);

	/*list<Client> res = dc.all();

	for (list<Client>::iterator it=res.begin(); it != res.end(); ++it)
	{
		cout << *it << endl;
	}*/

	//Client boublik("Maxxx","Bublikkk","Lvffiv");

	//dc.create(boublik);
	cout << dc.read(3) << endl;

	return 0;
}

int main1() {
   MYSQL *conn(0);
   MYSQL_RES *res;
   MYSQL_ROW row;
   char *server = "127.0.0.1";
   char *user = "root";
   char *password = "45xf4dl7"; /* set me first */
   char *database = "test";
   conn = mysql_init(NULL);
   /* Connect to database */
   if (!mysql_real_connect(conn, server,
         user, password, database, 3306, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return 0; 
   }
   /* send SQL query */
   if (mysql_query(conn, "select * from books")) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return 0;
   }
   res = mysql_use_result(conn);
   unsigned int num_fields(0);
   unsigned int i(0);
   num_fields = mysql_num_fields(res);
   while ((row = mysql_fetch_row(res)))
   {
   unsigned long *lengths;
   lengths = mysql_fetch_lengths(res);
   for(i = 0; i < num_fields; i++)
   {
       printf("%.*s ", (int) lengths[i],
              row[i] ? row[i] : "NULL");
   }
   printf("\n");
		/*cout << row[0] << ' ' << row[1] << ' ' << row[2];
		cout << ' ' << row[3] << ' ' << row[4] << endl;*/
   }
   /* close connection */
   mysql_free_result(res);
   mysql_close(conn);

   return 0;
}

//int main3() {
//MYSQL_STMT    *stmt;
//MYSQL_BIND    bind[3];
//my_ulonglong  affected_rows;
//int           param_count;
//short         small_data;
//int           int_data;
//char          str_data[STRING_SIZE];
//unsigned long str_length;
//my_bool       is_null;
//
//if (mysql_query(mysql, DROP_SAMPLE_TABLE))
//{
//  fprintf(stderr, " DROP TABLE failed\n");
//  fprintf(stderr, " %s\n", mysql_error(mysql));
//  exit(0);
//}
//
//if (mysql_query(mysql, CREATE_SAMPLE_TABLE))
//{
//  fprintf(stderr, " CREATE TABLE failed\n");
//  fprintf(stderr, " %s\n", mysql_error(mysql));
//  exit(0);
//}
//
///* Prepare an INSERT query with 3 parameters */
///* (the TIMESTAMP column is not named; the server */
///*  sets it to the current date and time) */
//stmt = mysql_stmt_init(mysql);
//if (!stmt)
//{
//  fprintf(stderr, " mysql_stmt_init(), out of memory\n");
//  exit(0);
//}
//if (mysql_stmt_prepare(stmt, INSERT_SAMPLE, strlen(INSERT_SAMPLE)))
//{
//  fprintf(stderr, " mysql_stmt_prepare(), INSERT failed\n");
//  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
//  exit(0);
//}
//fprintf(stdout, " prepare, INSERT successful\n");
//
///* Get the parameter count from the statement */
//param_count= mysql_stmt_param_count(stmt);
//fprintf(stdout, " total parameters in INSERT: %d\n", param_count);
//
//if (param_count != 3) /* validate parameter count */
//{
//  fprintf(stderr, " invalid parameter count returned by MySQL\n");
//  exit(0);
//}
//
///* Bind the data for all 3 parameters */
//
//memset(bind, 0, sizeof(bind));
//
///* INTEGER PARAM */
///* This is a number type, so there is no need
//   to specify buffer_length */
//bind[0].buffer_type= MYSQL_TYPE_LONG;
//bind[0].buffer= (char *)&int_data;
//bind[0].is_null= 0;
//bind[0].length= 0;
//
///* STRING PARAM */
//bind[1].buffer_type= MYSQL_TYPE_STRING;
//bind[1].buffer= (char *)str_data;
//bind[1].buffer_length= STRING_SIZE;
//bind[1].is_null= 0;
//bind[1].length= &str_length;
//
///* SMALLINT PARAM */
//bind[2].buffer_type= MYSQL_TYPE_SHORT;
//bind[2].buffer= (char *)&small_data;
//bind[2].is_null= &is_null;
//bind[2].length= 0;
//
///* Bind the buffers */
//if (mysql_stmt_bind_param(stmt, bind))
//{
//  fprintf(stderr, " mysql_stmt_bind_param() failed\n");
//  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
//  exit(0);
//}
//
///* Specify the data values for the first row */
//int_data= 10;             /* integer */
//strncpy(str_data, "MySQL", STRING_SIZE); /* string  */
//str_length= strlen(str_data);
//
///* INSERT SMALLINT data as NULL */
//is_null= 1;
//
///* Execute the INSERT statement - 1*/
//if (mysql_stmt_execute(stmt))
//{
//  fprintf(stderr, " mysql_stmt_execute(), 1 failed\n");
//  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
//  exit(0);
//}
//
///* Get the number of affected rows */
//affected_rows= mysql_stmt_affected_rows(stmt);
//fprintf(stdout, " total affected rows(insert 1): %lu\n",
//                (unsigned long) affected_rows);
//
//if (affected_rows != 1) /* validate affected rows */
//{
//  fprintf(stderr, " invalid affected rows by MySQL\n");
//  exit(0);
//}
//
///* Specify data values for second row,
//   then re-execute the statement */
//int_data= 1000;
//strncpy(str_data, "The most popular Open Source database",
//        STRING_SIZE);
//str_length= strlen(str_data);
//small_data= 1000;         /* smallint */
//is_null= 0;               /* reset */
//
///* Execute the INSERT statement - 2*/
//if (mysql_stmt_execute(stmt))
//{
//  fprintf(stderr, " mysql_stmt_execute, 2 failed\n");
//  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
//  exit(0);
//}
//
///* Get the total rows affected */
//affected_rows= mysql_stmt_affected_rows(stmt);
//fprintf(stdout, " total affected rows(insert 2): %lu\n",
//                (unsigned long) affected_rows);
//
//if (affected_rows != 1) /* validate affected rows */
//{
//  fprintf(stderr, " invalid affected rows by MySQL\n");
//  exit(0);
//}
//
///* Close the statement */
//if (mysql_stmt_close(stmt))
//{
//  fprintf(stderr, " failed while closing the statement\n");
//  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
//  exit(0);
//}
//
//return 0;
//}