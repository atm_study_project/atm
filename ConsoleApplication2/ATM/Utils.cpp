#include <iostream>
using namespace std;

#include "Utils.h"
#include <ctime>
#include <string>

bool Date::defaultSet(false);

Date Date::defaultDate(0, Date::Month(0),0);

const string Date::monthNames[12] =
{
	"January", "February", "March", "April",
	"May", "June", "July", "August", "September",
	"October", "November", "December"
};

size_t Date::monthLength[12] = 
{
	31, 28, 31, 30, 31, 30,
	31, 31, 30, 31, 30, 31
};

void Date::setDefault(size_t d, Month m, size_t y)
{
	defaultDate = Date(d, m, y);
}

void Date::setDefault()
{
	struct tm* today = new tm;
	time_t timer;
	time(&timer);
	gmtime_s(today, &timer);
	// choose current time
	defaultDate._day = today->tm_mday;
	defaultDate._month = ++(today->tm_mon);
	defaultDate._year = today->tm_year += 1900;
}

void Date::showDefault()
{
	cout << defaultDate << endl;
}


Date::Date(size_t d, Month m, size_t y)
{
	fillDate(d, m, y);
	return;
}

Date::Date(const Date& d):
	_day(d.getDay()), _month(d.getMonth()), _year(d.getMonth())
{
	return;
}
Date::~Date()
{
	return;
}

const string & Date::getMonthName(size_t m) const
{
	return monthNames[m - 1];
}

void Date::setDay(size_t d)
{
	if(d == 0 || monthLength[getMonth() - 1] < d && (leapYear(getYear()) && d != 29))
	{
		throw Date::BadDate(d, getMonth(), getYear());
	}
	_day = d;
}

void Date::setMonth(size_t m)
{
	if(m == 0 || m > 12)
	{
		throw Date::BadDate(getDay(), m, getYear());
	}
	_month = Month(m);
}
void Date::setYear(size_t y)
{
	if(y < 1900)
	{
		throw Date::BadDate(getDay(), getMonth(), y);
	}
	_year = y;
}

void Date::normalizeDate()
{
	Month month = getMonth();
	if(_day > monthLength[month - 1])
	{
		_day = 1;
		++_month;
		if(month > 12)
		{
			_month = Date::Month(1);
			++_year;		
		}
		return;
	}

	if(_day < 1)
	{
		--_month;
		if(month < 1)
		{
			_month = 12;
			--_year;
		}
		_day = monthLength[month - 1];
	}
}

bool Date::leapYear(size_t y)
{
	return (y % 4 == 0) && (y % 100 != 0) || (y % 400 == 0);
}

bool Date::corectDate(size_t d, Month m, size_t y)
{

	if((d < 1) || (monthLength[m - 1] < d) && (leapYear(y) && d != 29))
	{
		return false;
	}
	
	if(m < 1 || m > 12)
	{
		return false;
	}

	if(y < 1)
	{
		return false;
	}

	return true;
}

void Date::fillDate(const size_t& d, const Month& m, const size_t& y)
{
	if(!defaultSet)
	{
		defaultSet = true;
		setDefault();
	}

	_day = (d == 0 ? defaultDate._day : d);
	_month = (m == 0 ? defaultDate._month : m);
	_year = (y == 0 ? defaultDate._year : y);
	
	if(!corectDate(getDay(), getMonth(), getYear()))
	{
		throw Date::BadDate(getDay(), getMonth(), getYear());
	}
}

ostream& operator<<(ostream& os, const Date & d)
{
	os << d.getDay() << ' ';
	os << d.getMonthName(d.getMonth());
	os << ' ' << d.getYear();
	return os;
}

ostream& operator<<(ostream& os, const Date::BadDate & bd)
{
	os << "Bad day " << bd._day;
	os << ' ' << bd._month;
	os << ' ' << bd._year;
	return os;
}

