#include <iostream>
using namespace std;

#include <string>

#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "Utils.h"

class Client
{
private:
	size_t _id;
	string _first_name;
	string _last_name;
	string _adress;
	Date _date;

public:

	const static string tableName;
	const static string idField;
	const static string firstNameField;
	const static string lastNameField;
	const static string adressField;
	const static string birthdateField;

	Client(
		const string& fn="",
		const string& ln="",
		const string& adr="",
		const Date& d=Date()
		);
	Client(const Client&);
	~Client();

	inline const size_t & id() const {return _id;}
	inline const string & firstName() const {return _first_name;}
	inline const string & lastName() const {return _last_name;}
	inline const string & adress() const {return _adress;}
	inline const Date & date() const {return _date;}

	inline size_t & id() {return _id;}
	inline string & firstName() {return _first_name;}
	inline string & lastName() {return _last_name;}
	inline string & adress() {return _adress;}
	inline Date & date() {return _date;}

};

ostream& operator<<(ostream& os, const Client &);

#endif

#ifndef _CLIENT_DAO_H_
#define _CLIENT_DAO_H_

#include <memory>
#include <list>
#include "Dao.h"

class DaoClient: private virtual Dao<Client>
{
private:

	//struct ClientParamKeeper: public Dao<Client>::ParamKeeper
	//{
	//private:
	//	ClientParamKeeper(const ClientParamKeeper&);

	//public:
	//	size_t * id;
	//	char * first_name;
	//	char * last_name;
	//	char * adress;

	//	ClientParamKeeper(): ParamKeeper(){return;}
	//	~ClientParamKeeper(){return;}
	//};

	virtual list<Client> mapResultSet(MYSQL_RES *const&) const;
	//virtual list<Client> mapResultSet(MYSQL_STMT *const&, const std::list<ParamKeeper>&) const;

	/*void setBind(
		MYSQL_BIND &binding,
		const enum_field_types type,
		unsigned long buff_len,
		char *& buff,
		my_bool isNull=0
	) const;*/

public:
	explicit DaoClient(const shared_ptr<MYSQL>&);
	DaoClient(const DaoClient&);
	virtual ~DaoClient();

	virtual list<Client> all() const;
	virtual void create(const Client&) const;
	virtual Client read(size_t) const;
	virtual void update(const Client&) const;
	virtual void destroy(size_t) const;
};

#endif