#include <memory>
#include <cassert>
#include "Client.h"

const string Client::tableName("client");
const string Client::idField("id");
const string Client::firstNameField("first_name");
const string Client::lastNameField("last_name");
const string Client::adressField("adress");
const string Client::birthdateField("date");

Client::Client(const string& fn, const string& ln, const string& adr, const Date& d):
	_id(0), _first_name(fn), _last_name(ln), _adress(adr), _date(d)
{
	return;
}

Client::Client(const Client& client):
	_id(client.id()), _first_name(client.firstName()),
	_last_name(client.lastName()), _adress(client.adress()),
	_date(client.date())
{}

Client::~Client()
{
	return;
}

ostream& operator<<(ostream& os, const Client & client)
{
	os << client.id() << ' ' << client.firstName();
	os << ' ' << client.lastName() << ' ' << client.adress();
	os << ' ' << client.date();
	return os;
}

//	Dao client implementation

DaoClient::DaoClient(const shared_ptr<MYSQL>& conn):
	Dao<Client>(conn)
{
	return;
}

DaoClient::DaoClient(const DaoClient& dao):
	Dao<Client>(dao.connection())
{
	return;
}

DaoClient::~DaoClient()
{
	return;
}

list<Client> DaoClient::mapResultSet(MYSQL_RES *const& rs) const
{
	MYSQL_ROW row;
	list<Client> result;

	while ((row = mysql_fetch_row(rs)))
	{
		Client entity;
		entity.id() = std::stoi(row[0]);
		entity.firstName() = row[1];
		entity.lastName() = row[2];
		entity.adress() = row[3];
		
		result.push_back(entity);
	}

	return result;
}

//list<Client> DaoClient::mapResultSet(MYSQL_STMT *const& stmt, const std::list<ParamKeeper>& keepers) const
//{
//	list<Client> result;
//
//	for (list<ParamKeeper>::const_iterator it=keepers.begin(); it != keepers.end() && !mysql_stmt_fetch(stmt); ++it)
//	{
//		ClientParamKeeper tempK = static_cast<ParamKeeper>(*it);
//		
//		Client entity;
//		entity.id() = ;
//		entity.firstName() = row[1];
//		entity.lastName() = row[2];
//		entity.adress() = row[3];
//		
//		result.push_back(entity);
//	}
//	while (!mysql_stmt_fetch(stmt))
//	{
//		
//	}
//
//	return result;
//}

list<Client> DaoClient::all() const
{
	// creating query statement
	string query("SELECT * FROM ");
	query += Client::tableName;

	MYSQL *conn(Dao::connection().get());

	// Zero for success. Nonzero if an error occurred
	if(mysql_query(conn, query.c_str()))
	{
		throw Dao::MysqlException(mysql_error(conn));
	}

	MYSQL_RES *res(mysql_use_result(conn));
	list<Client> result(mapResultSet(res));
	mysql_free_result(res);

	return result;
}

void DaoClient::create(const Client& client) const
{
	const size_t fieldsNum(3);
	MYSQL_STMT *stmt(mysql_stmt_init(Dao::connection().get()));
	MYSQL_BIND param[fieldsNum];

	/* 
	'`' character is escape character
	for mysql;

	The original query is
	"INSERT INTO `client` SET
	`first_name`=?,`last_name`=?,
	`adress`=?"
	*/
	string query("INSERT INTO ");
	query += '`';
	query += Client::tableName;
	query += '`';
	query += " SET ";
	query += '`';
	query += Client::firstNameField;
	query += '`';
	query += "=?,";
	query += '`';
	query += Client::lastNameField;
	query += '`';
	query += "=?,";
	query += '`';
	query += Client::adressField;
	query += '`';
	query += "=?";
	
	if (mysql_stmt_prepare(stmt, query.c_str(), query.length())) 
	{
		throw DaoClient::MysqlException(mysql_stmt_error(stmt));
	}

	assert(mysql_stmt_param_count(stmt) == fieldsNum);

	//	initializing of MYSQL_BIND structure
	memset(param, 0, sizeof(param));
	
	unsigned long len1(client.firstName().length() + 1);
	char * v_prm1 = new char[len1];// string's method c_str() returns
	// const pointer to char, that's why it can't be used in assignment param[0].buffer=value
	strcpy_s(v_prm1, len1, client.firstName().c_str());
	//	setting the first sql '?' parameter
	param[0].buffer_type = MYSQL_TYPE_VARCHAR;
	param[0].buffer_length = len1;
	param[0].buffer = v_prm1;
	param[0].is_null = 0;
	param[0].length = &len1;
	
	unsigned long len2(client.lastName().length() + 1);
	char * v_prm2 = new char[len2];
	strcpy_s(v_prm2, len2, client.lastName().c_str());
	//	setting the second sql '?' parameter
	param[1].buffer_type = MYSQL_TYPE_VARCHAR;
	param[1].buffer_length = len2;
	param[1].buffer = v_prm2;
	param[1].is_null = 0;
	param[1].length = &len2;
	
	unsigned long len3(client.adress().length() + 1);
	char * v_prm3 = new char[len3];
	strcpy_s(v_prm3, len3, client.adress().c_str());
	//	setting the third sql '?' parameter
	param[2].buffer_type = MYSQL_TYPE_VARCHAR;
	param[2].buffer_length = len3;
	param[2].buffer = v_prm3;
	param[2].is_null = 0;
	param[2].length = &len3;

	//	note the order of functions
	if (mysql_stmt_bind_param(stmt, param) || mysql_stmt_execute(stmt) || mysql_stmt_close(stmt)) 
	{
		delete [] v_prm1; delete [] v_prm2; delete [] v_prm3;
		throw DaoClient::MysqlException(mysql_stmt_error(stmt));
	}

	delete [] v_prm1; delete [] v_prm2; delete [] v_prm3;
}

Client DaoClient::read(size_t id) const
{
	MYSQL_STMT *stmt(mysql_stmt_init(Dao::connection().get()));
	MYSQL_BIND result[4], param[1];
	
	string query("SELECT * FROM ");
	query += '`';
	query += Client::tableName;
	query += '`';
	query += " WHERE ";
	query += '`';
	query += Client::idField;
	query += '`';
	query += "=?";

	if (mysql_stmt_prepare(stmt, query.c_str(), query.length())) 
	{
		throw DaoClient::MysqlException(mysql_stmt_error(stmt));
	}

	assert(mysql_stmt_param_count(stmt) == 1);
	
	//	initializing of MYSQL_BIND structure
	memset(result, 0, sizeof(result));
	memset(param, 0, sizeof(param));

	// specifying query's bindings
	param[0].buffer_type = MYSQL_TYPE_LONG;
	param[0].buffer = (char *)&id;
	param[0].is_null = 0;
	param[0].length = 0;

	// binds params and tries to execute given statement
	if(mysql_stmt_bind_param(stmt, param) || mysql_stmt_execute(stmt))
	{
		throw DaoClient::MysqlException(mysql_stmt_error(stmt));
	}

	// should be the same as length
	// of corresponding field in the table
	const size_t max_buff_size(128);
	unsigned long buff_size[4];
	my_bool error[4];

	size_t c_id(0);
	char first_name[max_buff_size];
	char last_name[max_buff_size];
	char adress[max_buff_size];

	//	setting buffers
	result[0].buffer_type = MYSQL_TYPE_LONG;
	result[0].buffer = (char *) &c_id;
	result[0].is_null = 0;
	result[0].length = &buff_size[0];
	result[0].error = &error[0];

	result[1].buffer_type = MYSQL_TYPE_STRING;
	result[1].buffer = (char *) first_name;
	result[1].buffer_length = max_buff_size;
	result[1].is_null = 0;
	result[1].length = &buff_size[1];
	result[1].error = &error[1];

	result[2].buffer_type = MYSQL_TYPE_STRING;
	result[2].buffer = (char *) last_name;
	result[2].buffer_length = max_buff_size;
	result[2].is_null = 0;
	result[2].length = &buff_size[2];
	result[2].error = &error[2];

	result[3].buffer_type = MYSQL_TYPE_STRING;
	result[3].buffer = (char *) adress;
	result[3].buffer_length = max_buff_size;
	result[3].is_null = 0;
	result[3].length = &buff_size[3];
	result[3].error = &error[3];

	if(mysql_stmt_bind_result(stmt, result) != 0) 
	{
		throw DaoClient::MysqlException(mysql_stmt_error(stmt));
	}

	if (mysql_stmt_store_result(stmt))
	{
		throw DaoClient::MysqlException(mysql_stmt_error(stmt));
	}
	
	list<Client> clients;

	while(!mysql_stmt_fetch(stmt))
	{
		assert(first_name[buff_size[1]] = '\0');
		assert(last_name[buff_size[2]] = '\0');
		assert(adress[buff_size[3]] = '\0');

		Client client;
		client.id() = c_id;
		client.firstName() = first_name;
		client.lastName() = last_name;
		client.adress() = adress;

		clients.push_back(client);
	}

	// id should be unique
	if(clients.size() != 1)
	{
		throw DaoClient::MysqlException("multiple users with same id exist");
	}

	//mapResultSet(res);
	//mysql_free_result(res);

	if(mysql_stmt_close(stmt))
	{
		throw DaoClient::MysqlException(mysql_stmt_error(stmt));
	}

	return clients.front();
}

void DaoClient::update(const Client& client) const
{
	return;
}

void DaoClient::destroy(size_t) const
{
	return;
}

//void DaoClient::setBind(
//	MYSQL_BIND &binding,
//	const enum_field_types type,
//	unsigned long buff_len,
//	char *& buff,
//	my_bool isNull
//	) const
//{
//	binding.buffer_type = type;
//	binding.buffer_length = buff_len;
//	binding.buffer = buff;
//	binding.is_null = &isNull;
//	binding.length = &buff_len;
//}