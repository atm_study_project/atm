#ifndef _DAO_H_
#define _DAO_H_

#include <mysql.h>
#include <list>
#include <memory>
#include <string>

using namespace std;

/*
This class represents
abstract interface
for DAO;
E represents entity class
*/
template <class E>
class Dao
{
private:
	// connection representation
	std::shared_ptr<MYSQL> _connection;
	Dao & operator=(const Dao&);

public:
	
	// This class rerpesents 
	// errors which may occure
	// while processing db request/responce
	class MysqlException;

	explicit Dao(const std::shared_ptr<MYSQL>&);
	Dao(const Dao&);
	virtual ~Dao();

	// returns connection representation
	const std::shared_ptr<MYSQL> & connection() const;

	// returns list of all entities from database
	virtual list<E> all() const = 0;

	// creates a new record in database
	virtual void create(const E&) const = 0;

	// returns single record from database
	// which is identified by id
	virtual E read(size_t id) const = 0;

	// updates given entity in database
	virtual void update(const E&) const = 0;

	// destroys given entity which is identified
	// by id
	virtual void destroy(size_t id) const = 0;

protected:

	// maps given result set into the list of entities
	virtual list<E> mapResultSet(MYSQL_RES *const&) const = 0;
};

//template <class E>
//struct Dao<E>::ParamKeeper
//{
//public:
//	ParamKeeper(){return;}
//	virtual ~ParamKeeper(){return;}
//};

template <class E>
class Dao<E>::MysqlException
{
private:
	const string _info;

public:
	MysqlException(const string& info);
	~MysqlException();

	const string & info() const;
};

template <class E>
Dao<E>::Dao(const std::shared_ptr<MYSQL>& conn):
	_connection(conn)
{
	return;
}

template <class E>
Dao<E>::Dao(const Dao<E>& dao):
	_connection(dao.connection())
{
	return;
}

template <class E>
Dao<E>::~Dao()
{
	return;
}

template <class E>
const std::shared_ptr<MYSQL> & Dao<E>::connection() const
{
	return _connection;
}

template <class E>
Dao<E>::MysqlException::MysqlException(const string& info): 
	_info(info)
{
	return;
}

template <class E>
Dao<E>::MysqlException::~MysqlException()
{
	return;
}

template <class E>
const string & Dao<E>::MysqlException::info() const
{
	return _info;
}

#endif
