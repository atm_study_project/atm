
#ifndef _DATE_H
#define _DATE_H

#include <iostream>
#include <cstring>
using namespace std;

/*
Date representation class
*/
class Date
{
public:

struct BadDate;
enum Month {jan=1, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec};

static const string monthNames[12];
static size_t monthLength[12];

static void setDefault(size_t, Month, size_t);
static void setDefault();
static void showDefault();

static bool leapYear(size_t y);
static bool corectDate(size_t, Month, size_t);

Date (size_t d=0, Month m=Month(0), size_t y=0);
Date (const Date&);
~Date();

const string & getMonthName(size_t) const;

// Ugly getters and setters =(
inline const size_t & getDay() const {return _day;}
inline const Month getMonth() const {return Month(_month);}
inline const size_t & getYear() const {return _year;}

void setDay(size_t);
void setMonth(size_t);
void setYear(size_t);

private:

static bool defaultSet;
static Date defaultDate;

size_t _day, _month, _year;
void normalizeDate();
void fillDate(const size_t& d, const Month& m, const size_t& y);
};

ostream& operator<<(ostream& os, const Date &);

struct Date::BadDate
{
size_t _day, _month, _year;
BadDate(size_t d, size_t m, size_t y):_day(d), _month(m), _year(y){};
~BadDate(){};
};

std::ostream& operator<<(std::ostream& os, const Date::BadDate&);

#endif