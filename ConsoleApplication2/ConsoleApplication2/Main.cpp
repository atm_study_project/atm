#include <mysql.h>
#include <stdio.h>
#include <iostream>
using namespace std;

int main() {
   MYSQL *conn(0);
   MYSQL_RES *res;
   MYSQL_ROW row;
   char *server = "127.0.0.1";
   char *user = "root";
   char *password = "45xf4dl7"; /* set me first */
   char *database = "test";
   conn = mysql_init(NULL);
   /* Connect to database */
   if (!mysql_real_connect(conn, server,
         user, password, database, 3306, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return 0; 
   }
   /* send SQL query */
   if (mysql_query(conn, "select * from books")) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return 0;
   }
   res = mysql_use_result(conn);
   unsigned int num_fields(0);
   unsigned int i(0);
   num_fields = mysql_num_fields(res);
   while ((row = mysql_fetch_row(res)))
   {
   unsigned long *lengths;
   lengths = mysql_fetch_lengths(res);
   for(i = 0; i < num_fields; i++)
   {
       printf("%.*s ", (int) lengths[i],
              row[i] ? row[i] : "NULL");
   }
   printf("\n");
		/*cout << row[0] << ' ' << row[1] << ' ' << row[2];
		cout << ' ' << row[3] << ' ' << row[4] << endl;*/
   }
   /* close connection */
   mysql_free_result(res);
   mysql_close(conn);

   return 0;
}