/********************************************************************************
** Form generated from reading UI file 'atmgui.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ATMGUI_H
#define UI_ATMGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ATMgui
{
public:
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *keyboard;
    QPushButton *key1;
    QPushButton *key5;
    QPushButton *key2;
    QPushButton *key4;
    QPushButton *key8;
    QPushButton *key9;
    QPushButton *key3;
    QPushButton *key0;
    QPushButton *edit;
    QPushButton *delete_2;
    QPushButton *key6;
    QPushButton *cancel;
    QPushButton *key7;
    QPushButton *leftButton_1;
    QPushButton *leftButton_3;
    QPushButton *leftButton_2;
    QPushButton *leftButton_4;
    QLabel *dispencer;
    QPushButton *rightButton_3;
    QPushButton *rightButton_4;
    QPushButton *rightButton_2;
    QPushButton *rightButton_1;
    QStackedWidget *view;
    QLabel *label_3;
    QLabel *label_4;
    QTextEdit *receipt;
    QPushButton *pushButton;
    QGroupBox *groupBox;
    QTextEdit *card;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ATMgui)
    {
        if (ATMgui->objectName().isEmpty())
            ATMgui->setObjectName(QStringLiteral("ATMgui"));
        ATMgui->resize(800, 711);
        ATMgui->setStyleSheet(QStringLiteral(""));
        ATMgui->setDocumentMode(false);
        centralwidget = new QWidget(ATMgui);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        centralwidget->setEnabled(true);
        centralwidget->setStyleSheet(QStringLiteral("background: rgb(173, 174, 178);"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(170, 400, 351, 276));
        keyboard = new QGridLayout(gridLayoutWidget);
        keyboard->setObjectName(QStringLiteral("keyboard"));
        keyboard->setSizeConstraint(QLayout::SetDefaultConstraint);
        keyboard->setContentsMargins(0, 0, 0, 0);
        key1 = new QPushButton(gridLayoutWidget);
        key1->setObjectName(QStringLiteral("key1"));
        key1->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (1).jpg);\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold\n"
""));

        keyboard->addWidget(key1, 0, 0, 1, 1);

        key5 = new QPushButton(gridLayoutWidget);
        key5->setObjectName(QStringLiteral("key5"));
        key5->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (5).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key5, 1, 1, 1, 1);

        key2 = new QPushButton(gridLayoutWidget);
        key2->setObjectName(QStringLiteral("key2"));
        key2->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (2).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key2, 0, 1, 1, 1);

        key4 = new QPushButton(gridLayoutWidget);
        key4->setObjectName(QStringLiteral("key4"));
        key4->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (4).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key4, 1, 0, 1, 1);

        key8 = new QPushButton(gridLayoutWidget);
        key8->setObjectName(QStringLiteral("key8"));
        key8->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (8).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key8, 2, 1, 1, 1);

        key9 = new QPushButton(gridLayoutWidget);
        key9->setObjectName(QStringLiteral("key9"));
        key9->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (9).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold\n"
""));

        keyboard->addWidget(key9, 2, 2, 1, 1);

        key3 = new QPushButton(gridLayoutWidget);
        key3->setObjectName(QStringLiteral("key3"));
        key3->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (3).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key3, 0, 2, 1, 1);

        key0 = new QPushButton(gridLayoutWidget);
        key0->setObjectName(QStringLiteral("key0"));
        key0->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (0).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key0, 3, 1, 1, 1);

        edit = new QPushButton(gridLayoutWidget);
        edit->setObjectName(QStringLiteral("edit"));
        edit->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (clear).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:20px;\n"
"font-weight:bold"));

        keyboard->addWidget(edit, 0, 3, 1, 1);

        delete_2 = new QPushButton(gridLayoutWidget);
        delete_2->setObjectName(QStringLiteral("delete_2"));
        delete_2->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (delete).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:20px;\n"
"font-weight:bold"));

        keyboard->addWidget(delete_2, 1, 3, 1, 1);

        key6 = new QPushButton(gridLayoutWidget);
        key6->setObjectName(QStringLiteral("key6"));
        key6->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (6).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key6, 1, 2, 1, 1);

        cancel = new QPushButton(gridLayoutWidget);
        cancel->setObjectName(QStringLiteral("cancel"));
        cancel->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (cancel).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:20px;\n"
"font-weight:bold"));

        keyboard->addWidget(cancel, 2, 3, 1, 1);

        key7 = new QPushButton(gridLayoutWidget);
        key7->setObjectName(QStringLiteral("key7"));
        key7->setStyleSheet(QLatin1String("border-image: url(:/keypad1 (7).jpg) 0 0 0 0 stretch stretch;\n"
"width:30px;\n"
"height:50px;\n"
"\n"
"font-size:28px;\n"
"font-weight:bold"));

        keyboard->addWidget(key7, 2, 0, 1, 1);

        leftButton_1 = new QPushButton(centralwidget);
        leftButton_1->setObjectName(QStringLiteral("leftButton_1"));
        leftButton_1->setGeometry(QRect(104, 190, 41, 23));
        leftButton_1->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;\n"
""));
        leftButton_3 = new QPushButton(centralwidget);
        leftButton_3->setObjectName(QStringLiteral("leftButton_3"));
        leftButton_3->setGeometry(QRect(104, 250, 41, 23));
        leftButton_3->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        leftButton_2 = new QPushButton(centralwidget);
        leftButton_2->setObjectName(QStringLiteral("leftButton_2"));
        leftButton_2->setGeometry(QRect(104, 220, 41, 23));
        leftButton_2->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        leftButton_4 = new QPushButton(centralwidget);
        leftButton_4->setObjectName(QStringLiteral("leftButton_4"));
        leftButton_4->setGeometry(QRect(104, 280, 41, 23));
        leftButton_4->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        dispencer = new QLabel(centralwidget);
        dispencer->setObjectName(QStringLiteral("dispencer"));
        dispencer->setGeometry(QRect(170, 350, 351, 41));
        dispencer->setLayoutDirection(Qt::LeftToRight);
        dispencer->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        dispencer->setAlignment(Qt::AlignCenter);
        rightButton_3 = new QPushButton(centralwidget);
        rightButton_3->setObjectName(QStringLiteral("rightButton_3"));
        rightButton_3->setGeometry(QRect(540, 250, 41, 23));
        rightButton_3->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        rightButton_4 = new QPushButton(centralwidget);
        rightButton_4->setObjectName(QStringLiteral("rightButton_4"));
        rightButton_4->setGeometry(QRect(540, 280, 41, 23));
        rightButton_4->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        rightButton_2 = new QPushButton(centralwidget);
        rightButton_2->setObjectName(QStringLiteral("rightButton_2"));
        rightButton_2->setGeometry(QRect(540, 220, 41, 23));
        rightButton_2->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        rightButton_1 = new QPushButton(centralwidget);
        rightButton_1->setObjectName(QStringLiteral("rightButton_1"));
        rightButton_1->setGeometry(QRect(540, 190, 41, 23));
        rightButton_1->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        view = new QStackedWidget(centralwidget);
        view->setObjectName(QStringLiteral("view"));
        view->setGeometry(QRect(170, 80, 351, 251));
        view->setStyleSheet(QLatin1String("background-color:white;\n"
"\n"
""));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(670, 330, 91, 20));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(630, 360, 161, 21));
        label_4->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 1px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        receipt = new QTextEdit(centralwidget);
        receipt->setObjectName(QStringLiteral("receipt"));
        receipt->setEnabled(true);
        receipt->setGeometry(QRect(640, 370, 141, 161));
        receipt->setStyleSheet(QLatin1String("background-color:rgba(255, 255, 127, 242);\n"
"\n"
""));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(624, 80, 151, 31));
        pushButton->setStyleSheet(QLatin1String("background-color:green;\n"
"border: 2px dashed black;\n"
"color:white;\n"
"font-size:16px;\n"
"font-weight:bold;"));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(630, 130, 151, 81));
        card = new QTextEdit(groupBox);
        card->setObjectName(QStringLiteral("card"));
        card->setGeometry(QRect(0, 30, 141, 31));
        card->setStyleSheet(QStringLiteral("border:2;"));
        ATMgui->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(ATMgui);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        ATMgui->setStatusBar(statusbar);

        retranslateUi(ATMgui);

        QMetaObject::connectSlotsByName(ATMgui);
    } // setupUi

    void retranslateUi(QMainWindow *ATMgui)
    {
        ATMgui->setWindowTitle(QApplication::translate("ATMgui", "MainWindow", 0));
        key1->setText(QApplication::translate("ATMgui", "1", 0));
        key5->setText(QApplication::translate("ATMgui", "5", 0));
        key2->setText(QApplication::translate("ATMgui", "2", 0));
        key4->setText(QApplication::translate("ATMgui", "4", 0));
        key8->setText(QApplication::translate("ATMgui", "8", 0));
        key9->setText(QApplication::translate("ATMgui", "9", 0));
        key3->setText(QApplication::translate("ATMgui", "3", 0));
        key0->setText(QApplication::translate("ATMgui", "0", 0));
        edit->setText(QApplication::translate("ATMgui", "Edit", 0));
        delete_2->setText(QApplication::translate("ATMgui", "Delete", 0));
        key6->setText(QApplication::translate("ATMgui", "6", 0));
        cancel->setText(QApplication::translate("ATMgui", "Cancel", 0));
        key7->setText(QApplication::translate("ATMgui", "7", 0));
        leftButton_1->setText(QApplication::translate("ATMgui", "->", 0));
        leftButton_3->setText(QApplication::translate("ATMgui", "->", 0));
        leftButton_2->setText(QApplication::translate("ATMgui", "->", 0));
        leftButton_4->setText(QApplication::translate("ATMgui", "->", 0));
        dispencer->setText(QApplication::translate("ATMgui", "\320\222\320\270\320\264\320\260\320\262\320\260\321\202\320\270 \320\272\320\276\321\210\321\202\320\270", 0));
        rightButton_3->setText(QApplication::translate("ATMgui", "<-", 0));
        rightButton_4->setText(QApplication::translate("ATMgui", "<-", 0));
        rightButton_2->setText(QApplication::translate("ATMgui", "<-", 0));
        rightButton_1->setText(QApplication::translate("ATMgui", "<-", 0));
        label_3->setText(QApplication::translate("ATMgui", "\320\227\320\260\320\261\320\265\321\200\321\226\321\202\321\214 \321\201\320\262\321\226\320\271 \321\207\320\265\320\272", 0));
        label_4->setText(QApplication::translate("ATMgui", "---------------------", 0));
        receipt->setHtml(QApplication::translate("ATMgui", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">----------\320\222\320\260\321\210 \320\247\320\225\320\232------</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\320\227\320\275\321\217\321\202\321\202\321"
                        "\217 \320\263\320\276\321\202\321\226\320\262\320\272\320\270  100 \320\263\321\200\320\275</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">\320\222\320\241\320\254\320\236\320\223\320\236:          100 \320\263\321\200\320\275</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">-------\320\224\320\257\320\232\320\243\320\204\320\234\320\236------</p></body></html>", 0));
        pushButton->setText(QApplication::translate("ATMgui", "Insert card", 0));
        groupBox->setTitle(QApplication::translate("ATMgui", "Card", 0));
        card->setHtml(QApplication::translate("ATMgui", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class ATMgui: public Ui_ATMgui {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ATMGUI_H
