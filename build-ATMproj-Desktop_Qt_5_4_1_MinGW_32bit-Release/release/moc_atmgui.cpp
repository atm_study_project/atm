/****************************************************************************
** Meta object code from reading C++ file 'atmgui.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ATM/atmgui.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'atmgui.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ATMgui_t {
    QByteArrayData data[24];
    char stringdata[440];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ATMgui_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ATMgui_t qt_meta_stringdata_ATMgui = {
    {
QT_MOC_LITERAL(0, 0, 6), // "ATMgui"
QT_MOC_LITERAL(1, 7, 23), // "on_leftButton_1_clicked"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 23), // "on_leftButton_2_clicked"
QT_MOC_LITERAL(4, 56, 23), // "on_leftButton_3_clicked"
QT_MOC_LITERAL(5, 80, 23), // "on_leftButton_4_clicked"
QT_MOC_LITERAL(6, 104, 24), // "on_rightButton_1_clicked"
QT_MOC_LITERAL(7, 129, 24), // "on_rightButton_2_clicked"
QT_MOC_LITERAL(8, 154, 24), // "on_rightButton_3_clicked"
QT_MOC_LITERAL(9, 179, 24), // "on_rightButton_4_clicked"
QT_MOC_LITERAL(10, 204, 15), // "on_key1_clicked"
QT_MOC_LITERAL(11, 220, 15), // "on_key2_clicked"
QT_MOC_LITERAL(12, 236, 15), // "on_key3_clicked"
QT_MOC_LITERAL(13, 252, 15), // "on_key4_clicked"
QT_MOC_LITERAL(14, 268, 15), // "on_key5_clicked"
QT_MOC_LITERAL(15, 284, 15), // "on_key6_clicked"
QT_MOC_LITERAL(16, 300, 15), // "on_key7_clicked"
QT_MOC_LITERAL(17, 316, 15), // "on_key8_clicked"
QT_MOC_LITERAL(18, 332, 15), // "on_key9_clicked"
QT_MOC_LITERAL(19, 348, 15), // "on_key0_clicked"
QT_MOC_LITERAL(20, 364, 17), // "on_cancel_clicked"
QT_MOC_LITERAL(21, 382, 15), // "on_edit_clicked"
QT_MOC_LITERAL(22, 398, 19), // "on_delete_2_clicked"
QT_MOC_LITERAL(23, 418, 21) // "on_pushButton_clicked"

    },
    "ATMgui\0on_leftButton_1_clicked\0\0"
    "on_leftButton_2_clicked\0on_leftButton_3_clicked\0"
    "on_leftButton_4_clicked\0"
    "on_rightButton_1_clicked\0"
    "on_rightButton_2_clicked\0"
    "on_rightButton_3_clicked\0"
    "on_rightButton_4_clicked\0on_key1_clicked\0"
    "on_key2_clicked\0on_key3_clicked\0"
    "on_key4_clicked\0on_key5_clicked\0"
    "on_key6_clicked\0on_key7_clicked\0"
    "on_key8_clicked\0on_key9_clicked\0"
    "on_key0_clicked\0on_cancel_clicked\0"
    "on_edit_clicked\0on_delete_2_clicked\0"
    "on_pushButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ATMgui[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x08 /* Private */,
       3,    0,  125,    2, 0x08 /* Private */,
       4,    0,  126,    2, 0x08 /* Private */,
       5,    0,  127,    2, 0x08 /* Private */,
       6,    0,  128,    2, 0x08 /* Private */,
       7,    0,  129,    2, 0x08 /* Private */,
       8,    0,  130,    2, 0x08 /* Private */,
       9,    0,  131,    2, 0x08 /* Private */,
      10,    0,  132,    2, 0x08 /* Private */,
      11,    0,  133,    2, 0x08 /* Private */,
      12,    0,  134,    2, 0x08 /* Private */,
      13,    0,  135,    2, 0x08 /* Private */,
      14,    0,  136,    2, 0x08 /* Private */,
      15,    0,  137,    2, 0x08 /* Private */,
      16,    0,  138,    2, 0x08 /* Private */,
      17,    0,  139,    2, 0x08 /* Private */,
      18,    0,  140,    2, 0x08 /* Private */,
      19,    0,  141,    2, 0x08 /* Private */,
      20,    0,  142,    2, 0x08 /* Private */,
      21,    0,  143,    2, 0x08 /* Private */,
      22,    0,  144,    2, 0x08 /* Private */,
      23,    0,  145,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ATMgui::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ATMgui *_t = static_cast<ATMgui *>(_o);
        switch (_id) {
        case 0: _t->on_leftButton_1_clicked(); break;
        case 1: _t->on_leftButton_2_clicked(); break;
        case 2: _t->on_leftButton_3_clicked(); break;
        case 3: _t->on_leftButton_4_clicked(); break;
        case 4: _t->on_rightButton_1_clicked(); break;
        case 5: _t->on_rightButton_2_clicked(); break;
        case 6: _t->on_rightButton_3_clicked(); break;
        case 7: _t->on_rightButton_4_clicked(); break;
        case 8: _t->on_key1_clicked(); break;
        case 9: _t->on_key2_clicked(); break;
        case 10: _t->on_key3_clicked(); break;
        case 11: _t->on_key4_clicked(); break;
        case 12: _t->on_key5_clicked(); break;
        case 13: _t->on_key6_clicked(); break;
        case 14: _t->on_key7_clicked(); break;
        case 15: _t->on_key8_clicked(); break;
        case 16: _t->on_key9_clicked(); break;
        case 17: _t->on_key0_clicked(); break;
        case 18: _t->on_cancel_clicked(); break;
        case 19: _t->on_edit_clicked(); break;
        case 20: _t->on_delete_2_clicked(); break;
        case 21: _t->on_pushButton_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject ATMgui::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ATMgui.data,
      qt_meta_data_ATMgui,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ATMgui::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ATMgui::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ATMgui.stringdata))
        return static_cast<void*>(const_cast< ATMgui*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ATMgui::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
