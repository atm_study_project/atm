#-------------------------------------------------
#
# Project created by QtCreator 2015-11-30T00:00:05
#
#-------------------------------------------------

QT       += core gui uitools gui core
QT       += core sql

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ATMproj
TEMPLATE = app

CONFIG   -= app_bundle

SOURCES += main.cpp\
        atmgui.cpp \
        views/viewanothcard.cpp \
        views/viewoperations.cpp \
        views/viewpin.cpp \
        views/viewtransfer.cpp \
        views/viewwelcome.cpp \
        states/stateanothcard.cpp \
        states/stateoperations.cpp \
        states/statewelcome.cpp \
        states/statetransfer.cpp \
        states/statepin.cpp \
        states/statecashpayment.cpp \
        views/viewcashpayment.cpp \
        states/stateentersum.cpp \
        views/viewentersum.cpp \
    states/statephonepay.cpp \
    views/viewphonepay.cpp \
    views/viewerror.cpp \
    states/stateerror.cpp \
    states/statefinishcp.cpp \
    views/viewfinishcp.cpp \
    dao/accountdao.cpp \
    dao/carddao.cpp \
    dao/clientdao.cpp \
    dao/transactiondao.cpp \
    atm/account.cpp \
    atm/atm.cpp \
    atm/card.cpp \
    atm/cardreader.cpp \
    atm/cash.cpp \
    atm/Client.cpp \
    atm/creditcard.cpp \
    atm/datasource.cpp \
    atm/dispenser.cpp \
    atm/printer.cpp \
    atm/transaction.cpp \
    atm/receipt.cpp \
    views/viewamountcash.cpp \
    states/stateamountcash.cpp \
    views/viewsuccessfull.cpp \
    states/statesuccesfull.cpp \
    states/stateshowbalance.cpp \
    views/viewshowbalance.cpp \
    states/statephoneamount.cpp \
    views/viewphoneamount.cpp \
    views/viewadminatm.cpp \
    states/stateadminatm.cpp

HEADERS  += atmgui.h \
        states/state.h \
        states/stateanothcard.h \
        states/stateoperations.h \
        states/statepin.h \
        states/statetransfer.h \
        states/statewelcome.h \
        views/viewanothcard.h \
        views/viewoperations.h \
        views/viewpin.h \
        views/viewtransfer.h \
        views/viewwelcome.h \
        views/screen.h \
        views/viewcashpayment.h \
        states/statecashpayment.h \
        states/stateentersum.h \
        views/viewentersum.h \
    states/statephonepay.h \
    views/viewphonepay.h \
    states/stateerror.h \
    views/viewerror.h \
    states/statefinishcp.h \
    views/viewfinishcp.h \
    dao/AccountDao.h \
    dao/CardDao.h \
    dao/ClientDao.h \
    dao/Dao.h \
    dao/transactiondao.h \
    atm/Account.h \
    atm/atm.h \
    atm/bill.h \
    atm/Card.h \
    atm/cardreader.h \
    atm/cash.h \
    atm/Client.h \
    atm/creditcard.h \
    atm/datasource.h \
    atm/dispenser.h \
    atm/printer.h \
    atm/Transaction.h \
    atm/receipt.h \
    states/stateamontcash.h \
    views/viewamountcash.h \
    states/statesuccessfull.h \
    views/viewsuccessfull.h \
    states/stateshowbalance.h \
    views/viewshowbalance.h \
    states/statephoneamount.h \
    views/viewphoneamount.h \
    states/stateadminatm.h \
    views/viewadminatm.h
FORMS    += atmgui.ui


RESOURCES += \
    forms.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/mysql-driver/ -lqsqlmysql4
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/mysql-driver/ -lqsqlmysql4d
else:unix: LIBS += -L$$PWD/mysql-driver/ -lqsqlmysql4

INCLUDEPATH += $$PWD/mysql-driver
DEPENDPATH += $$PWD/mysql-driver

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/mysql-driver/ -lqsqlmysqld4
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/mysql-driver/ -lqsqlmysqld4d
else:unix: LIBS += -L$$PWD/mysql-driver/ -lqsqlmysqld4

INCLUDEPATH += $$PWD/mysql-driver
DEPENDPATH += $$PWD/mysql-driver
