#include <QtWidgets>
#include <QtUiTools>
#include "views/viewcashpayment.h"
#include "views/screen.h"
#include <cassert>

ViewCashPayment::ViewCashPayment():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewCashPayment::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\cashpayment.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
   // _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewCashPayment::setEditField(const QString& str)
{
   // _pinTextEdit->setText(str);
    return;
}

const QString ViewCashPayment::getEditField()
{
    return _pinTextEdit->toPlainText();
}



