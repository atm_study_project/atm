#ifndef VIEWSUCCESSFULL
#define VIEWSUCCESSFULL

#include <QWidget>
#include <QLineEdit>
#include "screen.h"

class ViewSuccessfull : public Screen
{
private:
    QLineEdit * _pinTextEdit;

public:
    ViewSuccessfull();
    virtual ~ViewSuccessfull(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWSUCCESSFULL

