#ifndef VIEWTRANSFER
#define VIEWTRANSFER

#include <QWidget>
#include <QTextEdit>
#include "screen.h"

class ViewTransfer : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewTransfer();
    virtual ~ViewTransfer(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWTRANSFER

