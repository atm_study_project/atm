#ifndef VIEWERROR
#define VIEWERROR
#include "views/screen.h"
#include <QTextEdit>

class ViewError : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewError();
    virtual ~ViewError(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};


#endif // VIEWERROR

