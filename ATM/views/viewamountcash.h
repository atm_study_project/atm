#ifndef VIEWAMOUNTCASH
#define VIEWAMOUNTCASH
#include "views/screen.h"

class ViewAmountCash : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewAmountCash();
    virtual ~ViewAmountCash(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};


#endif // VIEWAMOUNTCASH

