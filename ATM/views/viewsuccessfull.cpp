#include <QtWidgets>
#include <QtUiTools>
#include "views/viewsuccessfull.h"
#include "views/screen.h"
#include <cassert>

ViewSuccessfull::ViewSuccessfull():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewSuccessfull::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\successfull.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();

    return formWidget;
}

void ViewSuccessfull::setEditField(const QString& )
{
}

const QString ViewSuccessfull::getEditField()
{
    return "";
}



