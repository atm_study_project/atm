
#include <QtWidgets>
#include <QtUiTools>
#include "views/viewamountcash.h"
#include "views/screen.h"
#include <cassert>

ViewAmountCash::ViewAmountCash():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewAmountCash::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\amountcash.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewAmountCash::setEditField(const QString& str)
{
    _pinTextEdit->setText(str);
}

const QString ViewAmountCash::getEditField()
{
    return _pinTextEdit->toPlainText();
}



