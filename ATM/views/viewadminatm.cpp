
#include <QtWidgets>
#include <QtUiTools>
#include "views/viewadminatm.h"
#include "views/screen.h"
#include <cassert>

ViewAdminATM::ViewAdminATM():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewAdminATM::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\adminatm.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    //_pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewAdminATM::setEditField(const QString& str)
{
    //_pinTextEdit->setText(str);
}

const QString ViewAdminATM::getEditField()
{
    return "";//_pinTextEdit->toPlainText();
}


