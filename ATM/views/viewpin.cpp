#include <QtWidgets>
#include <QtUiTools>
#include <QString>
#include "views/viewpin.h"
#include "views/screen.h"
#include <cassert>

ViewPIN::ViewPIN():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewPIN::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\pin.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QLineEdit *>("editableField");

    return formWidget;
}

void ViewPIN::setEditField(const QString& str)
{
    _pinTextEdit->setText(str);
}

const QString ViewPIN::getEditField()
{
     QTextStream cout(stdout);
     cout<<_pinTextEdit->text();
    return _pinTextEdit->text();//toPlainText();
}

