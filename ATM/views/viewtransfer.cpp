#include <QtWidgets>
#include <QtUiTools>
#include "views/viewtransfer.h"
#include "views/screen.h"
#include <cassert>

ViewTransfer::ViewTransfer():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewTransfer::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\transfer.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewTransfer::setEditField(const QString& str)
{
    _pinTextEdit->setText(str);
}

const QString ViewTransfer::getEditField()
{
    return _pinTextEdit->toPlainText();
}


