#ifndef SCREEN
#define SCREEN
#include <QWidget>

class Screen:public QWidget
{
public:
    Screen(): QWidget(){}
    virtual ~Screen(){}
    virtual QWidget* loadUiFile() = 0;
    virtual void  setEditField(const QString& ) = 0;
    virtual const QString getEditField() = 0;
};

#endif // SCREEN

