
#include <QtWidgets>
#include <QtUiTools>
#include "views/viewphonepay.h"
#include "views/screen.h"
#include <cassert>

ViewPhonePay::ViewPhonePay():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewPhonePay::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\phonepay.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewPhonePay::setEditField(const QString& str)
{
    _pinTextEdit->setText(str);
}

const QString ViewPhonePay::getEditField()
{
    return _pinTextEdit->toPlainText();
}



