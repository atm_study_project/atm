#include <QtWidgets>
#include <QtUiTools>
#include "views/viewoperations.h"
#include "views/screen.h"
#include <cassert>

ViewOperations::ViewOperations():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewOperations::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\operations.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
   // _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewOperations::setEditField(const QString& str)
{
   // _pinTextEdit->setText(str);
    return;
}

const QString ViewOperations::getEditField()
{
    return _pinTextEdit->toPlainText();
}

