#ifndef VIEWSHOWBALANCE
#define VIEWSHOWBALANCE
#include <QTextEdit>
#include "views/screen.h"
class ViewShowBalance : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewShowBalance();
    virtual ~ViewShowBalance(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWSHOWBALANCE

