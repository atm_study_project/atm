#ifndef VIEWPHONEPAY
#define VIEWPHONEPAY

#include <QWidget>
#include <QTextEdit>
#include "screen.h"

class ViewPhonePay : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewPhonePay();
    virtual ~ViewPhonePay(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWPHONEPAY

