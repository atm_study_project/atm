#ifndef VIEWPIN
#define VIEWPIN

#include <QWidget>
#include <QLineEdit>
#include "screen.h"

class ViewPIN : public Screen
{
private:
    QLineEdit * _pinTextEdit;

public:
    ViewPIN();
    virtual ~ViewPIN(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWPIN

