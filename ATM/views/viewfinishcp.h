#ifndef VIEWFINISHCP
#define VIEWFINISHCP

#include <QWidget>
#include <QTextEdit>
#include "screen.h"

class ViewFinishCP : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewFinishCP();
    virtual ~ViewFinishCP(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWFINISHCP

