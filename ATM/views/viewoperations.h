#ifndef VIEWOPERATIONS
#define VIEWOPERATIONS
#include <QWidget>
#include <QTextEdit>
#include "screen.h"

class ViewOperations : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewOperations();
    virtual ~ViewOperations(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWOPERATIONS

