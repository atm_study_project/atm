#include <QtWidgets>
#include <QtUiTools>
#include "views/viewwelcome.h"
#include "views/screen.h"

ViewWelcome::ViewWelcome():Screen()
{

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewWelcome::loadUiFile()
{
    QUiLoader loader;

    QTextStream out(stdout);
    out << QString("Path: "+QDir::currentPath());
        QFile file(":\\welcome.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();


    return formWidget;
}

void ViewWelcome::setEditField(const QString& str)
{
str;
}

const QString ViewWelcome::getEditField()
{
    return "";
}
