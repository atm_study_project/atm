#include <QtWidgets>
#include <QtUiTools>
#include "views/viewfinishcp.h"
#include "views/screen.h"
#include <cassert>

ViewFinishCP::ViewFinishCP():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewFinishCP::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\finishcashpayment.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
   // _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewFinishCP::setEditField(const QString& str)
{
   // _pinTextEdit->setText(str);
    return;
}

const QString ViewFinishCP::getEditField()
{
    return _pinTextEdit->toPlainText();
}



