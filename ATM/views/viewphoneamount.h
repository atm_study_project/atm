#ifndef VIEWPHONEAMOUNT
#define VIEWPHONEAMOUNT
#include "views/screen.h"
#include <QTextEdit>
class ViewPhoneAmount : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewPhoneAmount();
    virtual ~ViewPhoneAmount(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWPHONEAMOUNT

