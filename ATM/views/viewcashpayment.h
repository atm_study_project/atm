#ifndef VIEWCASHPAYMENT
#define VIEWCASHPAYMENT

#include <QWidget>
#include <QTextEdit>
#include "screen.h"

class ViewCashPayment : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewCashPayment();
    virtual ~ViewCashPayment(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWCASHPAYMENT

