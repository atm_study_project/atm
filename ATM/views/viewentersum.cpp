
#include <QtWidgets>
#include <QtUiTools>
#include "views/viewentersum.h"
#include "views/screen.h"
#include <cassert>

ViewEnterSum::ViewEnterSum():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewEnterSum::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\entersum.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewEnterSum::setEditField(const QString& str)
{
    _pinTextEdit->setText(str);
}

const QString ViewEnterSum::getEditField()
{
    return _pinTextEdit->toPlainText();
}



