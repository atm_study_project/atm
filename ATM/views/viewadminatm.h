#ifndef VIEWADMINATM
#define VIEWADMINATM

#include "views/screen.h"

class ViewAdminATM : public Screen
{
//private:

public:
    ViewAdminATM();
    virtual ~ViewAdminATM(){return;}
    virtual QWidget* loadUiFile();
    virtual void  setEditField(const QString& );
    virtual const QString getEditField();

};

#endif // VIEWADMINATM

