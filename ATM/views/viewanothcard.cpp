
#include <QtWidgets>
#include <QtUiTools>
#include "views/viewanothcard.h"
#include "views/screen.h"
#include <cassert>

ViewAnothCard::ViewAnothCard():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewAnothCard::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\anothercard.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewAnothCard::setEditField(const QString& str)
{
    _pinTextEdit->setText(str);
}

const QString ViewAnothCard::getEditField()
{
    return _pinTextEdit->toPlainText();
}


