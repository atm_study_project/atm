#ifndef VIEWENTERSUM
#define VIEWENTERSUM

#include <QWidget>
#include <QTextEdit>
#include "screen.h"

class ViewEnterSum : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewEnterSum();
    virtual ~ViewEnterSum(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWENTERSUM

