#include <QtWidgets>
#include <QtUiTools>
#include "views/viewerror.h"
#include "views/screen.h"
#include <cassert>

ViewError::ViewError():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewError::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\error.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("error");

    return formWidget;
}

void ViewError::setEditField(const QString& str)
{
     _pinTextEdit->setText(str);
    return;
}

const QString ViewError::getEditField()
{
    return _pinTextEdit->toPlainText();
}



