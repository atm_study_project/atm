#include <QtWidgets>
#include <QtUiTools>
#include "views/viewshowbalance.h"
#include "views/screen.h"

ViewShowBalance::ViewShowBalance():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewShowBalance::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\showbalance.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewShowBalance::setEditField(const QString& str)
{
   _pinTextEdit->setText(str);
}

const QString ViewShowBalance::getEditField()
{
    return _pinTextEdit->toPlainText();
}



