
#include <QtWidgets>
#include <QtUiTools>
#include "views/viewphoneamount.h"
#include "views/screen.h"
#include <cassert>

ViewPhoneAmount::ViewPhoneAmount():Screen()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(loadUiFile());
    setLayout(mainLayout);
}

QWidget* ViewPhoneAmount::loadUiFile()
{
    QUiLoader loader;

    QFile file(":\\phoneamount.ui");
    file.open(QFile::ReadOnly);

    QWidget *formWidget = loader.load(&file, this);
    file.close();
    _pinTextEdit = formWidget->findChild<QTextEdit *>("editableField");

    return formWidget;
}

void ViewPhoneAmount::setEditField(const QString& str)
{
    _pinTextEdit->setText(str);
}

const QString ViewPhoneAmount::getEditField()
{
    return _pinTextEdit->toPlainText();
}



