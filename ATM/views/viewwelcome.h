#ifndef VIEWWELCOME
#define VIEWWELCOME

#include <QWidget>
#include "screen.h"

class ViewWelcome : public Screen
{
public:
    ViewWelcome();
    virtual ~ViewWelcome(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};


#endif // VIEWWELCOME

