#ifndef VIEWANOTHCARD
#define VIEWANOTHCARD

#include <QWidget>
#include <QTextEdit>
#include "screen.h"

class ViewAnothCard : public Screen
{
private:
    QTextEdit * _pinTextEdit;

public:
    ViewAnothCard();
    virtual ~ViewAnothCard(){return;}
    virtual QWidget* loadUiFile();
    virtual void setEditField(const QString& str);
    virtual const QString getEditField();
};

#endif // VIEWANOTHCARD

