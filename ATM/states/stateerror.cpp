#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/stateoperations.h"
#include "states/state.h"
#include "views/screen.h"
#include "views/viewoperations.h"
#include "views/viewpin.h"
#include "views/viewwelcome.h"

#include "states/stateoperations.h"
#include "states/stateerror.h"
#include "states/statewelcome.h"
#include "states/statepin.h"

void StateError::doDelete()
{
    _screen.setEditField("");
}

     void StateError::doEdit()
     {
         QString currStr = _screen.getEditField();
         _screen.setEditField(currStr.left(currStr.size()- 1));
     }

     void StateError::doCancel()
     {
          Screen *vp = new ViewWelcome();
          State *sp = new StateWelcome(*vp,_parent);
          _parent.changePage(sp);

          // returning card
          _parent.returnCard();
     }

     void StateError::input(const QString& str)
     {
         _screen.setEditField(_screen.getEditField()+str);
     }

    StateError::StateError(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent){}
    StateError::~StateError(){}

     const Screen& StateError::screen() const {return _screen;}
     Screen& StateError::screen() {return _screen;}

     void StateError::onButton_1(){}

     void StateError::onButton_2(){}

     void StateError::onButton_3(){}

     void StateError::onButton_4(){}

     void StateError::onButton_5(){}

     void StateError::onButton_6(){}

     void StateError::onButton_7(){}

     void StateError::onButton_8(){}

     void StateError::onButton_9(){}

     void StateError::onButton_0(){}

    //Delete last character in editable field
     void StateError::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateError::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateError::onButton_Cancel(){doCancel();}

     void StateError::onLButton_1(){}
     void StateError::onLButton_2(){}
     void StateError::onLButton_3(){}
     void StateError::onLButton_4()
     {
         //button: CANCEL
         // go to main menu
         if(_parent.getATM().isAuthorized())
         {

             Screen *vp = new ViewOperations();
             State *sp = new StateOperations(*vp,_parent);
              _parent.changePage(sp);
         }
         else
         {
             Screen *vp = new ViewPIN();
             State *sp = new StatePIN(*vp,_parent);
              _parent.changePage(sp);
         }

         QTextStream cout(stdout);
         cout << "change to PIN screen";
     }

     void StateError::onRButton_1(){}
     void StateError::onRButton_2(){}
     void StateError::onRButton_3(){}
     void StateError::onRButton_4(){}




