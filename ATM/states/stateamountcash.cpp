#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/stateoperations.h"
#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewsuccessfull.h"
#include "views/viewerror.h"
#include "states/stateerror.h"
#include "views/viewwelcome.h"

#include "states/statepin.h"
#include "states/stateamontcash.h"
#include "states/statewelcome.h"
#include "states/statesuccessfull.h"

     void StateAmountCash::doDelete()
     {
        _screen.setEditField("");
     }

     void StateAmountCash::doEdit()
     {
         QString currStr = _screen.getEditField();
         _screen.setEditField(currStr.left(currStr.size()- 1));
     }

     void StateAmountCash::doCancel()
     {
          Screen *vp = new ViewWelcome();
          State *sp = new StateWelcome(*vp,_parent);
          _parent.changePage(sp);

          // returning card
          _parent.returnCard();
     }

     void StateAmountCash::input(const QString& str)
     {
         size_t len = _screen.getEditField().size();
         if(len < 5)
         {
              _screen.setEditField(_screen.getEditField() + str);
         }
     }

    StateAmountCash::StateAmountCash(Screen &scr, ATMgui& parent, long long card):
        State(scr,parent), _screen(scr), _parent(parent),_cardNum(card){}

    StateAmountCash::~StateAmountCash(){}

     const Screen& StateAmountCash::screen() const {return _screen;}
     Screen& StateAmountCash::screen() {return _screen;}

     void StateAmountCash::onButton_1(){input("1");}

     void StateAmountCash::onButton_2(){input("2");}

     void StateAmountCash::onButton_3(){input("3");}

     void StateAmountCash::onButton_4(){input("4");}

     void StateAmountCash::onButton_5(){input("5");}

     void StateAmountCash::onButton_6(){input("6");}

     void StateAmountCash::onButton_7(){input("7");}

     void StateAmountCash::onButton_8(){input("8");}

     void StateAmountCash::onButton_9(){input("9");}

     void StateAmountCash::onButton_0(){input("0");}

    //Delete last character in editable field
     void StateAmountCash::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateAmountCash::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateAmountCash::onButton_Cancel(){doCancel();}

     void StateAmountCash::onLButton_1(){}
     void StateAmountCash::onLButton_2(){}
     void StateAmountCash::onLButton_3(){}
     void StateAmountCash::onLButton_4(){}

     void StateAmountCash::onRButton_1(){}

     void StateAmountCash::onRButton_2(){}

     void StateAmountCash::onRButton_3(){}
     void StateAmountCash::onRButton_4()
     {
         QTextStream cout(stdout);
         unsigned int amount(_screen.getEditField().toUInt());
         //Не можна дістати дані про картку лежить в CardReader вже після того
         //як ми авторизувалися
         //Помилка фатальна, швидше за все втратили звязок з базою
         if(!_parent.getATM().validCardAccount()) {
             doCancel();
         }
         //Недостатньо грошей
         else if(!_parent.getATM().balanceExceeds(amount)) {
             Screen *ve = new ViewError();
             State *se = new StateError(*ve,_parent);
             ve->setEditField("Requested transfer amount exceeds account balance");
             _parent.changePage(se);
         }
         //Введено неправильні дані
         else if(!_parent.getATM().validCardAccount(_cardNum)) {
             Screen *ve = new ViewError();
             State *se = new StateError(*ve,_parent);
             ve->setEditField("Entered card account does not exist");
             _parent.changePage(se);
         }
         else {
             try {
                 _parent.getATM().transferToAccount(_cardNum, amount);
                 Screen *vs = new ViewSuccessfull();
                 State *ss = new StateSuccessfull(*vs,_parent);
                 _parent.changePage(ss);
                 cout << "change to Successfull Payment screen";
             }
             //Можлива помилка при записі даних в базу
             //Ніяк пояснити користувачу не можемо
             catch(const ATM::ATMException& exc) {
                 Screen *ve = new ViewError();
                 State *se = new StateError(*ve,_parent);
                 ve->setEditField("Unknown error. Please try again later");
                 _parent.changePage(se);
             }
         }
    }



