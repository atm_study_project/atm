#ifndef STATEOPERATIONS
#define STATEOPERATIONS

#include "states/state.h"
#include "views/screen.h"

class StateOperations: public State
{
     Screen &_screen;
     ATMgui &_parent;

     void doDelete();
     void doEdit();
     void doCancel();
     void input(const QString& str);

public:
    StateOperations(Screen &scr, ATMgui& parent);
    ~StateOperations();

    virtual const Screen& screen() const ;
    virtual Screen& screen();

    virtual void onButton_1();

    virtual void onButton_2();

    virtual void onButton_3();

    virtual void onButton_4();

    virtual void onButton_5();

    virtual void onButton_6();

    virtual void onButton_7();

    virtual void onButton_8();

    virtual void onButton_9();

    virtual void onButton_0();

    //Delete last character in editable field
    virtual void onButton_Edit();
    //Clearing text in editable field on Screen
    virtual void onButton_Delete();
    //Cancelling and returning to Screen with PIN code field
    virtual void onButton_Cancel();

    virtual void onLButton_1();
    virtual void onLButton_2();
    virtual void onLButton_3();
    virtual void onLButton_4();

    virtual void onRButton_1();
    virtual void onRButton_2();
    virtual void onRButton_3();
    virtual void onRButton_4();

};


#endif // STATEOPERATIONS

