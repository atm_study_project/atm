#include <QtCore/QCoreApplication>
#include <QTextStream>

#include "states/state.h"
#include "views/screen.h"
#include "states/stateadminatm.h"
#include "states/statewelcome.h"
#include "views/viewwelcome.h"


     void StateAdminATM::doDelete()
     {
        _screen.setEditField("");
     }

     void StateAdminATM::doEdit()
     {
         QString currStr = _screen.getEditField();
         _screen.setEditField(currStr.left(currStr.size()- 1));
     }

     void StateAdminATM::doCancel()
     {
          Screen *vp = new ViewWelcome();
          State *sp = new StateWelcome(*vp,_parent);
          _parent.changePage(sp);

          // returning card
          _parent.returnCard();
     }

     void StateAdminATM::input(const QString& str)
     {
         size_t len = _screen.getEditField().size();
         if(len < 5)
         {
              _screen.setEditField(_screen.getEditField() + str);
         }
     }

    StateAdminATM::StateAdminATM(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent){}

    StateAdminATM::~StateAdminATM(){}

     const Screen& StateAdminATM::screen() const {return _screen;}
     Screen& StateAdminATM::screen() {return _screen;}

     void StateAdminATM::onButton_1(){}

     void StateAdminATM::onButton_2(){}

     void StateAdminATM::onButton_3(){}

     void StateAdminATM::onButton_4(){}

     void StateAdminATM::onButton_5(){}

     void StateAdminATM::onButton_6(){}

     void StateAdminATM::onButton_7(){}

     void StateAdminATM::onButton_8(){}

     void StateAdminATM::onButton_9(){}

     void StateAdminATM::onButton_0(){}

    //Delete last character in editable field
     void StateAdminATM::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateAdminATM::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateAdminATM::onButton_Cancel(){doCancel();}

     void StateAdminATM::onLButton_1(){}
     void StateAdminATM::onLButton_2(){}
     void StateAdminATM::onLButton_3(){}
     void StateAdminATM::onLButton_4(){}

     void StateAdminATM::onRButton_1(){}
     void StateAdminATM::onRButton_2(){}
     void StateAdminATM::onRButton_3(){}
     void StateAdminATM::onRButton_4(){}




