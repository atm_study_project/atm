#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/stateoperations.h"
#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewoperations.h"
#include "views/viewerror.h"
#include "states/stateerror.h"
#include "views/viewwelcome.h"

#include "states/statepin.h"
#include "stateanothcard.h"
#include "states/statewelcome.h"
#include "states/stateamontcash.h"
#include "views/viewamountcash.h"

     void StateAnothCard::doDelete()
     {
        _screen.setEditField("");
     }

     void StateAnothCard::doEdit()
     {
         QString currStr = _screen.getEditField();
         _screen.setEditField(currStr.left(currStr.size()- 1));
     }

     void StateAnothCard::doCancel()
     {
          Screen *vp = new ViewWelcome();
          State *sp = new StateWelcome(*vp,_parent);
          _parent.changePage(sp);
          _parent.returnCard();
     }

     void StateAnothCard::input(const QString& str)
     {
         size_t len = _screen.getEditField().size();
         if(len < 16)
         {
              _screen.setEditField(_screen.getEditField() + str);
         }
     }


    StateAnothCard::StateAnothCard(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent){}
    StateAnothCard::~StateAnothCard(){}

     const Screen& StateAnothCard::screen() const {return _screen;}
     Screen& StateAnothCard::screen() {return _screen;}

     void StateAnothCard::onButton_1(){input("1");}

     void StateAnothCard::onButton_2(){input("2");}

     void StateAnothCard::onButton_3(){input("3");}

     void StateAnothCard::onButton_4(){input("4");}

     void StateAnothCard::onButton_5(){input("5");}

     void StateAnothCard::onButton_6(){input("6");}

     void StateAnothCard::onButton_7(){input("7");}

     void StateAnothCard::onButton_8(){input("8");}

     void StateAnothCard::onButton_9(){input("9");}

     void StateAnothCard::onButton_0(){input("0");}

    //Delete last character in editable field
     void StateAnothCard::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateAnothCard::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateAnothCard::onButton_Cancel(){doCancel();}

     void StateAnothCard::onLButton_1(){}
     void StateAnothCard::onLButton_2()
     {

     }
     void StateAnothCard::onLButton_3(){}
     void StateAnothCard::onLButton_4(){}

     void StateAnothCard::onRButton_1(){}
     void StateAnothCard::onRButton_2()
     {
         QTextStream cout(stdout);
         if(_screen.getEditField().size() == 16 || true)
         {
             Screen *vo = new ViewAmountCash();
             State *so = new StateAmountCash(*vo,_parent,_screen.getEditField().toLongLong());
             _parent.changePage(so);
             cout << "change to AmountCash screen";
         }
         else
         {
             Screen *ve = new ViewError();
             State *se = new StateError(*ve,_parent);
             _parent.changePage(se);
             ve->setEditField("eeeerrrrrrooooorrrrr");
             cout << "change to Error screen";

         }

     }

     void StateAnothCard::onRButton_3(){}
     void StateAnothCard::onRButton_4()
    {


    }


