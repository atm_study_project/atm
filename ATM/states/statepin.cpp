#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewoperations.h"
#include "states/stateoperations.h"
#include "states/statepin.h"
#include "states/stateerror.h"
#include "views/viewerror.h"
#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/statewelcome.h"
#include "views/viewwelcome.h"

    void StatePIN::doDelete()
    {
       _screen.setEditField("");
    }

    void StatePIN::doEdit()
    {
        QString currStr = _screen.getEditField();
        _screen.setEditField(currStr.left(currStr.size()- 1));
    }

    void StatePIN::doCancel()
    {
         Screen *vp = new ViewWelcome();
         State *sp = new StateWelcome(*vp,_parent);

         _parent.changePage(sp);
         //returning card
         _parent.returnCard();
    }

    void StatePIN::input(const QString& str)
    {
        size_t len = _screen.getEditField().size();
        if(len < 4)
        {
             _screen.setEditField(_screen.getEditField() + str);
        }
    }

    StatePIN::StatePIN(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent){}
    StatePIN::~StatePIN(){}

     const Screen& StatePIN::screen() const {return _screen;}
     Screen& StatePIN::screen() {return _screen;}

     void StatePIN::onButton_1(){input("1");}

     void StatePIN::onButton_2(){input("2");}

     void StatePIN::onButton_3(){input("3");}

     void StatePIN::onButton_4(){input("4");}

     void StatePIN::onButton_5(){input("5");}

     void StatePIN::onButton_6(){input("6");}

     void StatePIN::onButton_7(){input("7");}

     void StatePIN::onButton_8(){input("8");}

     void StatePIN::onButton_9(){input("9");}

     void StatePIN::onButton_0(){input("0");}

    //Delete last character in editable field
     void StatePIN::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StatePIN::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StatePIN::onButton_Cancel(){doCancel();}

     void StatePIN::onLButton_1(){}
     void StatePIN::onLButton_2(){}
     void StatePIN::onLButton_3(){}
     void StatePIN::onLButton_4(){}

     void StatePIN::onRButton_1(){}
     void StatePIN::onRButton_2(){}
     void StatePIN::onRButton_3(){}
     void StatePIN::onRButton_4()
    {
        QTextStream cout(stdout);
        if(!_parent.getATM().validCardAccount()) {
            Screen *ve = new ViewError();
            State *se = new StateError(*ve,_parent);
            _parent.changePage(se);
            ve->setEditField("Entered card account does not exist");
        }
        else if(_screen.getEditField().size() != 4) {
            Screen *ve = new ViewError();
            State *se = new StateError(*ve,_parent);
            _parent.changePage(se);
            ve->setEditField("PIN must consist of 4 digits");
        }
        else if(_parent.getATM().checkPin(_screen.getEditField().toInt())) {
            Screen *vo = new ViewOperations();
            State *so = new StateOperations(*vo,_parent);
            _parent.changePage(so);
            cout << "change to Operations screen";
        }
        else {
            // Тут треба рахувати кількість спроб
            Screen *ve = new ViewError();
            State *se = new StateError(*ve,_parent);
            _parent.changePage(se);
            ve->setEditField("Incorrect PIN");
        }
    }

