#include "states/state.h"
#include "views/screen.h"
#include "views/viewshowbalance.h"
#include "views/viewoperations.h"
#include "states/stateoperations.h"
#include "states/stateshowbalance.h"
#include "states/stateerror.h"
#include "views/viewerror.h"
#include "states/statepin.h"
#include "views/viewpin.h"
#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/statewelcome.h"
#include "views/viewwelcome.h"

    void StateShowBalance::doDelete()
    {
       _screen.setEditField("");
    }

    void StateShowBalance::doEdit()
    {
        QString currStr = _screen.getEditField();
        _screen.setEditField(currStr.left(currStr.size()- 1));
    }

    void StateShowBalance::doCancel()
    {
         Screen *vp = new ViewWelcome();
         State *sp = new StateWelcome(*vp,_parent);
         _parent.changePage(sp);
         //returning card
         _parent.returnCard();

    }

    void StateShowBalance::input(const QString& str)
    {
        size_t len = _screen.getEditField().size();
        if(len < 4)
        {
             _screen.setEditField(_screen.getEditField() + str);
        }
    }

    StateShowBalance::StateShowBalance(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent)
    {
        _screen.setEditField(QString::number(_parent.getATM().checkBalance(), 'f', 2));
      // _parent.getATM().checkBalance();
    }

    StateShowBalance::~StateShowBalance(){}

     const Screen& StateShowBalance::screen() const {return _screen;}
     Screen& StateShowBalance::screen() {return _screen;}

     void StateShowBalance::onButton_1(){input("1");}

     void StateShowBalance::onButton_2(){input("2");}

     void StateShowBalance::onButton_3(){input("3");}

     void StateShowBalance::onButton_4(){input("4");}

     void StateShowBalance::onButton_5(){input("5");}

     void StateShowBalance::onButton_6(){input("6");}

     void StateShowBalance::onButton_7(){input("7");}

     void StateShowBalance::onButton_8(){input("8");}

     void StateShowBalance::onButton_9(){input("9");}

     void StateShowBalance::onButton_0(){input("0");}

    //Delete last character in editable field
     void StateShowBalance::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateShowBalance::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateShowBalance::onButton_Cancel(){doCancel();}

     void StateShowBalance::onLButton_1(){}
     void StateShowBalance::onLButton_2(){}
     void StateShowBalance::onLButton_3(){}
     void StateShowBalance::onLButton_4()
     {
         QTextStream cout(stdout);
         Screen *vo = new ViewOperations();
         State *so = new StateOperations(*vo,_parent);
         _parent.changePage(so);
         cout << "change to Operations screen";
     }

     void StateShowBalance::onRButton_1(){}
     void StateShowBalance::onRButton_2(){}
     void StateShowBalance::onRButton_3(){}
     void StateShowBalance::onRButton_4()
    {
        QTextStream cout(stdout);
            Screen *vo = new ViewPIN();
            State *so = new StatePIN(*vo,_parent);
            _parent.changePage(so);
            cout << "change to PIN screen";
            //returning card
            _parent.returnCard();

    }


