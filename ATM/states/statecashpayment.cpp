#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/stateoperations.h"
#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewoperations.h"
#include "views/viewerror.h"
#include "views/viewsuccessfull.h"
#include "views/viewwelcome.h"

#include "states/statepin.h"
#include "states/statecashpayment.h"
#include "states/statewelcome.h"
#include "views/viewentersum.h"
#include "states/stateentersum.h"
#include "states/stateerror.h"
#include "states/statesuccessfull.h"
#include "states/stateshowbalance.h"
#include "views/viewshowbalance.h"

#include "assert.h"


void StateCashPayment::doDelete()
{
    _screen.setEditField("");
}

void StateCashPayment::doEdit()
{
    QString currStr = _screen.getEditField();
    _screen.setEditField(currStr.left(currStr.size()- 1));
}

void StateCashPayment::doCancel()
{
    Screen *vp = new ViewWelcome();
    State *sp = new StateWelcome(*vp,_parent);
    _parent.changePage(sp);

    _parent.returnCard();

}

void StateCashPayment::input(const QString& )
{
}

StateCashPayment::StateCashPayment(Screen &scr, ATMgui& parent):
    State(scr,parent), _screen(scr), _parent(parent)
{}
StateCashPayment::~StateCashPayment(){}

const Screen& StateCashPayment::screen() const {return _screen;}
Screen& StateCashPayment::screen() {return _screen;}

void StateCashPayment::onButton_1(){}

void StateCashPayment::onButton_2(){}

void StateCashPayment::onButton_3(){}

void StateCashPayment::onButton_4(){}

void StateCashPayment::onButton_5(){}

void StateCashPayment::onButton_6(){}

void StateCashPayment::onButton_7(){}

void StateCashPayment::onButton_8(){}

void StateCashPayment::onButton_9(){}

void StateCashPayment::onButton_0(){}

//Delete last character in editable field
void StateCashPayment::onButton_Edit(){doEdit();}
//Clearing text in editable field on Screen
void StateCashPayment::onButton_Delete(){doDelete();}
//Cancelling and returning to Screen with PIN code field
void StateCashPayment::onButton_Cancel(){doCancel();}

void StateCashPayment::onLButton_1()
{
    doWithdraw(100);
}
void StateCashPayment::onLButton_2()
{
    doWithdraw(200);
}
void StateCashPayment::onLButton_3()
{
    doWithdraw(500);
}
void StateCashPayment::onLButton_4() {}

void StateCashPayment::onRButton_1()
{
    Screen *vo = new ViewEnterSum();
    State *so = new StateEnterSum(*vo,_parent);

    _parent.changePage(so);
}
void StateCashPayment::onRButton_2()
{
    Screen *sb = new ViewShowBalance();
    State *stbal = new StateShowBalance(*sb, _parent);

    _parent.changePage(stbal);

}

void StateCashPayment::onRButton_3()
{
    Screen *vo = new ViewOperations();
    State *so = new StateOperations(*vo,_parent);

    _parent.changePage(so);

}
void StateCashPayment::onRButton_4()
{
    //button: CANCEL
    // go to main menu

    Screen *vo = new ViewPIN();
    State *so = new StatePIN(*vo,_parent);

    _parent.changePage(so);

}

void StateCashPayment::doWithdraw(unsigned int amount) //const
{
    ATM &atm = _parent.getATM();

    Screen *view(0);
    State *state(0);

    //Фатальна помилка картки, потрібно знову спробувати авторизуватися
    if (!atm.validCardAccount()) {
        doCancel();
    }
    //Не можемо видати таку суму фізично
    else if(!atm.canDispense(amount)) {
        view = new ViewError();
        state = new StateError(*view,_parent);
        view->setEditField("Cannot dispense entered amount");
    }
    //Недостатньо коштів
    else if(!atm.balanceExceeds(amount)) {
        view = new ViewError();
        state = new StateError(*view,_parent);
        view->setEditField("Requested withdrawal amount exceeds account balance");
    }
    else {
        try
        {
            Cash cash = atm.withdraw(amount);
            view = new ViewSuccessfull();
            state = new StateSuccessfull(*view, _parent);
            view->setEditField("Operation was completed successfully");
            _parent.dispenceCash(cash);
        }
        catch(const ATM::ATMException& ex)
        {
            view = new ViewError();
            state = new StateError(*view,_parent);
            view->setEditField("Unknown error. Please try again later");
        }
    }

    assert(state && view);
    _parent.changePage(state);
}



