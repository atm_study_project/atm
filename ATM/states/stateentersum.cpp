#include <QtCore/QCoreApplication>
#include <QTextStream>

#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewoperations.h"
#include "views/viewfinishcp.h"
#include "views/viewerror.h"
#include "views/viewsuccessfull.h"
#include "views/viewwelcome.h"

#include "states/statepin.h"
#include "states/stateentersum.h"
#include "states/statewelcome.h"
#include "states/statefinishcp.h"
#include "states/stateoperations.h"
#include "states/state.h"
#include "states/stateerror.h"
#include "states/statesuccessfull.h"

#include "assert.h"


     void StateEnterSum::doDelete()
     {
        _screen.setEditField("");
     }

     void StateEnterSum::doEdit()
     {
         QString currStr = _screen.getEditField();
         _screen.setEditField(currStr.left(currStr.size()- 1));
     }

     void StateEnterSum::doCancel()
     {
          Screen *vp = new ViewWelcome();
          State *sp = new StateWelcome(*vp,_parent);
          _parent.changePage(sp);
          // to do return card
     }

     void StateEnterSum::input(const QString& str)
     {
         _screen.setEditField(_screen.getEditField()+str);
     }

    StateEnterSum::StateEnterSum(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent)
        {}

    StateEnterSum::~StateEnterSum(){}

     const Screen& StateEnterSum::screen() const {return _screen;}
     Screen& StateEnterSum::screen() {return _screen;}

     void StateEnterSum::onButton_1(){input("1");}

     void StateEnterSum::onButton_2(){input("2");}

     void StateEnterSum::onButton_3(){input("3");}

     void StateEnterSum::onButton_4(){input("4");}

     void StateEnterSum::onButton_5(){input("5");}

     void StateEnterSum::onButton_6(){input("6");}

     void StateEnterSum::onButton_7(){input("7");}

     void StateEnterSum::onButton_8(){input("8");}

     void StateEnterSum::onButton_9(){input("9");}

     void StateEnterSum::onButton_0(){input("0");}

    //Delete last character in editable field
     void StateEnterSum::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateEnterSum::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateEnterSum::onButton_Cancel(){doCancel();}

     void StateEnterSum::onLButton_1(){}
     void StateEnterSum::onLButton_2(){}
     void StateEnterSum::onLButton_3(){}
     void StateEnterSum::onLButton_4(){}

     void StateEnterSum::onRButton_1()
     {
         //button: CANCEL
         // go to main menu

         if(_screen.getEditField().length() != 0)
         {
            doWithdraw(_screen.getEditField().toInt());
         }
     }
     void StateEnterSum::onRButton_2(){}
     void StateEnterSum::onRButton_3(){}
     void StateEnterSum::onRButton_4()
    {
        //button: CANCEL
        // go to main menu
         doCancel();
    }

     void StateEnterSum::doWithdraw(unsigned int amount)
     {
         ATM &atm = _parent.getATM();

         Screen *view(0);
         State *state(0);

         //Фатальна помилка картки, потрібно знову спробувати авторизуватися
         if (!atm.validCardAccount()) {
             doCancel();
         }
         //Не можемо видати таку суму фізично
         else if(!atm.canDispense(amount)) {
             view = new ViewError();
             state = new StateError(*view,_parent);
             view->setEditField("Cannot dispense entered amount");
         }
         //Недостатньо коштів
         else if(!atm.balanceExceeds(amount)) {
             view = new ViewError();
             state = new StateError(*view,_parent);
             view->setEditField("Requested withdrawal amount exceeds account balance");
         }
         else {
             try
             {
                 Cash cash = atm.withdraw(amount);
                 view = new ViewSuccessfull();
                 state = new StateSuccessfull(*view, _parent);
                 view->setEditField("Operation was completed successfully");
                 _parent.dispenceCash(cash);
             }
             catch(const ATM::ATMException& ex)
             {
                 view = new ViewError();
                 state = new StateError(*view,_parent);
                 view->setEditField("Unknown error. Please try again later");
             }
         }

         assert(state && view);
         _parent.changePage(state);
     }



