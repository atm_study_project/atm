#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/stateoperations.h"
#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewsuccessfull.h"
#include "views/viewerror.h"
#include "states/stateerror.h"
#include "views/viewwelcome.h"

#include "states/statepin.h"
#include "states/statephoneamount.h"
#include "views/viewphoneamount.h"
#include "states/statewelcome.h"
#include "states/statesuccessfull.h"

     void StatePhoneAmount::doDelete()
     {
        _screen.setEditField("");
     }

     void StatePhoneAmount::doEdit()
     {
         QString currStr = _screen.getEditField();
         _screen.setEditField(currStr.left(currStr.size()- 1));
     }

     void StatePhoneAmount::doCancel()
     {
          Screen *vp = new ViewWelcome();
          State *sp = new StateWelcome(*vp,_parent);
          _parent.changePage(sp);

          // returning card
          _parent.returnCard();
     }

     void StatePhoneAmount::input(const QString& str)
     {
         size_t len = _screen.getEditField().size();
         if(len < 5)
         {
              _screen.setEditField(_screen.getEditField() + str);
         }
     }

    StatePhoneAmount::StatePhoneAmount(Screen &scr, ATMgui& parent, QString number):
        State(scr,parent), _screen(scr), _parent(parent),_mobileNum(number){}

    StatePhoneAmount::~StatePhoneAmount(){}

     const Screen& StatePhoneAmount::screen() const {return _screen;}
     Screen& StatePhoneAmount::screen() {return _screen;}

     void StatePhoneAmount::onButton_1(){input("1");}

     void StatePhoneAmount::onButton_2(){input("2");}

     void StatePhoneAmount::onButton_3(){input("3");}

     void StatePhoneAmount::onButton_4(){input("4");}

     void StatePhoneAmount::onButton_5(){input("5");}

     void StatePhoneAmount::onButton_6(){input("6");}

     void StatePhoneAmount::onButton_7(){input("7");}

     void StatePhoneAmount::onButton_8(){input("8");}

     void StatePhoneAmount::onButton_9(){input("9");}

     void StatePhoneAmount::onButton_0(){input("0");}

    //Delete last character in editable field
     void StatePhoneAmount::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StatePhoneAmount::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StatePhoneAmount::onButton_Cancel(){doCancel();}

     void StatePhoneAmount::onLButton_1(){}
     void StatePhoneAmount::onLButton_2(){}
     void StatePhoneAmount::onLButton_3(){}
     void StatePhoneAmount::onLButton_4(){}

     void StatePhoneAmount::onRButton_1(){}

     void StatePhoneAmount::onRButton_2(){}

     void StatePhoneAmount::onRButton_3(){}
     void StatePhoneAmount::onRButton_4()
     {
        QTextStream cout(stdout);

         unsigned int amount(_screen.getEditField().toUInt());
         //Не можна дістати дані про картку лежить в CardReader вже після того
         //як ми авторизувалися
         //Помилка фатальна, швидше за все втратили звязок з базою
         if(!_parent.getATM().validCardAccount()) {
             doCancel();
         }
         //Недостатньо грошей
         else if(!_parent.getATM().balanceExceeds(amount)) {
             Screen *ve = new ViewError();
             State *se = new StateError(*ve,_parent);
             ve->setEditField("Requested transfer amount exceeds account balance");
             _parent.changePage(se);
         }
         else {
             try {
                 _parent.getATM().transferToMobile(_mobileNum, amount);

                 Screen *vs = new ViewSuccessfull();
                 State *ss = new StateSuccessfull(*vs,_parent);
                 _parent.changePage(ss);
                 cout << "change to Successfull Payment screen";
             }
             //Можлива помилка при записі даних в базу
             //Ніяк пояснити користувачу не можемо
             catch(const ATM::ATMException& exc) {
                 Screen *ve = new ViewError();
                 State *se = new StateError(*ve,_parent);
                 ve->setEditField("Unknown error. Please try again later");
                 _parent.changePage(se);
             }
         }
    }




