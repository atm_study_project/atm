#include <QtCore/QCoreApplication>
#include <QTextStream>

#include "states/state.h"
#include "views/screen.h"
#include "views/viewtransfer.h"
#include "states/statetransfer.h"
#include "states/statephonepay.h"
#include "states/statecashpayment.h"
#include "views/viewcashpayment.h"
#include "views/viewpin.h"
#include "states/statepin.h"
#include "views/viewphoneamount.h"
#include "states/statephoneamount.h"
#include "states/statewelcome.h"
#include "views/viewwelcome.h"
#include "atmgui.h"


void StatePhonePay::doDelete()
{
   _screen.setEditField("");
}

void StatePhonePay::doEdit()
{
    size_t len = _screen.getEditField().size();
    if(len > 8)
    {
        QString currStr = _screen.getEditField();
        _screen.setEditField(currStr.left(currStr.size()- 1));
    }
}

void StatePhonePay::doCancel()
{
     Screen *vp = new ViewWelcome();
     State *sp = new StateWelcome(*vp,_parent);
     _parent.changePage(sp);

     // returning card
     _parent.returnCard();
}

void StatePhonePay::input(const QString& str)
{
    if(_screen.getEditField().size() < 17)
    {
        _screen.setEditField(_screen.getEditField()+str);
    }
}

    StatePhonePay::StatePhonePay(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent)
    {
        return;
    }

    StatePhonePay::~StatePhonePay(){}

     const Screen& StatePhonePay::screen() const {return _screen;}
     Screen& StatePhonePay::screen() {return _screen;}

     void StatePhonePay::onButton_1(){input("1");}

     void StatePhonePay::onButton_2(){input("2");}

     void StatePhonePay::onButton_3(){input("3");}

     void StatePhonePay::onButton_4(){input("4");}

     void StatePhonePay::onButton_5(){input("5");}

     void StatePhonePay::onButton_6(){input("6");}

     void StatePhonePay::onButton_7(){input("7");}

     void StatePhonePay::onButton_8(){input("8");}

     void StatePhonePay::onButton_9(){input("9");}

     void StatePhonePay::onButton_0(){input("0");}

    //Delete last character in editable field
     void StatePhonePay::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StatePhonePay::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StatePhonePay::onButton_Cancel(){doCancel();}

     void StatePhonePay::onLButton_1()
    {
        Screen *vo = new ViewTransfer();
        State *so = new StateTransfer(*vo,_parent);

       _parent.changePage(so);
        QTextStream cout(stdout);
            cout << "change to Transfer screen";
    }
     void StatePhonePay::onLButton_2(){}
     void StatePhonePay::onLButton_3(){}
     void StatePhonePay::onLButton_4(){}
     void StatePhonePay::onRButton_1(){}
void StatePhonePay::onRButton_2()
{
    Screen *vo = new ViewCashPayment();
    State *so = new StateCashPayment(*vo,_parent);

   _parent.changePage(so);
    QTextStream cout(stdout);
        cout << "change to Cash Paynent screen";
}
     void StatePhonePay::onRButton_3(){}
     void StatePhonePay::onRButton_4()
     {
         Screen *vo = new ViewPhoneAmount();
         State *so = new StatePhoneAmount(*vo,_parent,_screen.getEditField());

        _parent.changePage(so);
         QTextStream cout(stdout);
             cout << "change to Phone Amount screen";
     }






