#include "states/state.h"
#include "views/screen.h"
#include "views/viewtransfer.h"
#include "views/viewanothcard.h"
#include "atmgui.h"
#include "states/statetransfer.h"
#include "states/stateanothcard.h"
#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/statetransfer.h"
#include "views/viewpin.h"
#include "views/viewwelcome.h"
#include "states/statewelcome.h"


    void StateTransfer::doDelete()
    {
        QString currStr = _screen.getEditField();
        _screen.setEditField(currStr+"1");
    }

    void StateTransfer::doEdit(){}
    void StateTransfer::doCancel()
    {
        Screen *vp = new ViewWelcome();
        State *sp = new StateWelcome(*vp,_parent);
        _parent.changePage(sp);

        _parent.returnCard();

    }

    StateTransfer::StateTransfer(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent)
    {
        return;
    }

    StateTransfer::~StateTransfer(){}

     const Screen& StateTransfer::screen() const {return _screen;}
     Screen& StateTransfer::screen() {return _screen;}

     void StateTransfer::onButton_1() {}
     void StateTransfer::onButton_2() {}
     void StateTransfer::onButton_3() {}
     void StateTransfer::onButton_4() {}
     void StateTransfer::onButton_5() {}
     void StateTransfer::onButton_6() {}
     void StateTransfer::onButton_7() {}
     void StateTransfer::onButton_8() {}
     void StateTransfer::onButton_9() {}
     void StateTransfer::onButton_0() {}
     void StateTransfer::onButton_Edit() {}
     void StateTransfer::onButton_Delete() {}
     void StateTransfer::onButton_Cancel() {}

     void StateTransfer::onLButton_1(){}

     void StateTransfer::onLButton_2(){}
     void StateTransfer::onLButton_3(){}
     void StateTransfer::onLButton_4(){}
     void StateTransfer::onRButton_1(){}
     void StateTransfer::onRButton_2()
    {
        Screen *vo = new ViewAnothCard();
        State *so = new StateAnothCard(*vo,_parent);
        _parent.changePage(so);
        QTextStream cout(stdout);
        cout << "change to AnotherCard screen";
    }

     void StateTransfer::onRButton_3(){}
     void StateTransfer::onRButton_4(){}


