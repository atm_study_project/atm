#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewoperations.h"
#include "states/stateoperations.h"
#include "states/statepin.h"
#include "states/stateerror.h"
#include "views/viewerror.h"
#include "views/viewsuccessfull.h"
#include "states/statesuccessfull.h"
#include "states/stateshowbalance.h"
#include "views/viewshowbalance.h"
#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/statewelcome.h"
#include "views/viewwelcome.h"

    void StateSuccessfull::doDelete()
    {
       _screen.setEditField("");
    }

    void StateSuccessfull::doEdit()
    {
        QString currStr = _screen.getEditField();
        _screen.setEditField(currStr.left(currStr.size()- 1));
    }

    void StateSuccessfull::doCancel()
    {
         Screen *vp = new ViewWelcome();
         State *sp = new StateWelcome(*vp,_parent);
         _parent.changePage(sp);

         // returning card
         _parent.returnCard();
    }

    void StateSuccessfull::input(const QString& str)
    {
        size_t len = _screen.getEditField().size();
        if(len < 4)
        {
             _screen.setEditField(_screen.getEditField() + str);
        }
    }

    StateSuccessfull::StateSuccessfull(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent){}
    StateSuccessfull::~StateSuccessfull(){}

     const Screen& StateSuccessfull::screen() const {return _screen;}
     Screen& StateSuccessfull::screen() {return _screen;}

     void StateSuccessfull::onButton_1(){}

     void StateSuccessfull::onButton_2(){}

     void StateSuccessfull::onButton_3(){}

     void StateSuccessfull::onButton_4(){}

     void StateSuccessfull::onButton_5(){}

     void StateSuccessfull::onButton_6(){}

     void StateSuccessfull::onButton_7(){}

     void StateSuccessfull::onButton_8(){}

     void StateSuccessfull::onButton_9(){}

     void StateSuccessfull::onButton_0(){}

    //Delete last character in editable field
     void StateSuccessfull::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateSuccessfull::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateSuccessfull::onButton_Cancel(){doCancel();}

     void StateSuccessfull::onLButton_1(){}
     void StateSuccessfull::onLButton_2(){}
     void StateSuccessfull::onLButton_3(){}
     void StateSuccessfull::onLButton_4()
     {
         // show balance
         Screen * vs = new ViewShowBalance();
         State * ss = new StateShowBalance(*vs,_parent);
         _parent.changePage(ss);
     }

     void StateSuccessfull::onRButton_1(){}
     void StateSuccessfull::onRButton_2(){}
     void StateSuccessfull::onRButton_3(){}
     void StateSuccessfull::onRButton_4()
     {
         //printing receipt
         _parent.printReceipt();
     }

