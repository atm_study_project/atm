#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "atmgui.h"
#include "states/statepin.h"
#include "states/statewelcome.h"


    StateWelcome::StateWelcome(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent)
    {
        return;
    }
    StateWelcome::~StateWelcome(){}

     const Screen& StateWelcome::screen() const {return _screen;}
     Screen& StateWelcome::screen() {return _screen;}

     void StateWelcome::onButton_1() {}
     void StateWelcome::onButton_2() {}
     void StateWelcome::onButton_3() {}
     void StateWelcome::onButton_4() {}
     void StateWelcome::onButton_5() {}
     void StateWelcome::onButton_6() {}
     void StateWelcome::onButton_7() {}
     void StateWelcome::onButton_8() {}
     void StateWelcome::onButton_9() {}
     void StateWelcome::onButton_0() {}
     void StateWelcome::onButton_Edit() {}
     void StateWelcome::onButton_Delete() {}
     void StateWelcome::onButton_Cancel() {}


     void StateWelcome::onLButton_1(){}

     void StateWelcome::onLButton_2(){}
     void StateWelcome::onLButton_3(){}
     void StateWelcome::onLButton_4(){}
     void StateWelcome::onRButton_1(){}
     void StateWelcome::onRButton_2(){}
     void StateWelcome::onRButton_3(){}
     void StateWelcome::onRButton_4(){}

