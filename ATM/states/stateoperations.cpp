#include <QtCore/QCoreApplication>
#include <QTextStream>

#include "states/state.h"
#include "views/screen.h"
#include "views/viewtransfer.h"
#include "states/statetransfer.h"
#include "states/stateoperations.h"
#include "states/statecashpayment.h"
#include "views/viewcashpayment.h"
#include "states/statephonepay.h"
#include "views/viewphonepay.h"
#include "views/viewshowbalance.h"
#include "states/stateshowbalance.h"
#include "states/statewelcome.h"
#include "views/viewwelcome.h"
#include "atmgui.h"

    void StateOperations::doDelete()
    {
        QString currStr = _screen.getEditField();
        _screen.setEditField(currStr+"1");
    }

    void StateOperations::doEdit(){}

    void StateOperations::doCancel()
    {
         Screen *vp = new ViewWelcome();
         State *sp = new StateWelcome(*vp,_parent);
         _parent.changePage(sp);

         // returning card
         _parent.returnCard();
    }


    StateOperations::StateOperations(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent)
    {
        return;
    }

    StateOperations::~StateOperations(){}

     const Screen& StateOperations::screen() const {return _screen;}
     Screen& StateOperations::screen() {return _screen;}

     void StateOperations::onButton_1() {}

     void StateOperations::onButton_2() {}

     void StateOperations::onButton_3() {}
     void StateOperations::onButton_4() {}
     void StateOperations::onButton_5() {}
     void StateOperations::onButton_6() {}
     void StateOperations::onButton_7() {}
     void StateOperations::onButton_8() {}
     void StateOperations::onButton_9() {}
     void StateOperations::onButton_0() {}
     void StateOperations::onButton_Edit() {}
     void StateOperations::onButton_Delete() {}
     void StateOperations::onButton_Cancel() {doCancel();}

     void StateOperations::onLButton_1()
    {
        Screen *vo = new ViewTransfer();
        State *so = new StateTransfer(*vo,_parent);

       _parent.changePage(so);
        QTextStream cout(stdout);
            cout << "change to Transfer screen";
    }
     void StateOperations::onLButton_2()
     {
         Screen *vo = new ViewPhonePay();
         State *so = new StatePhonePay(*vo,_parent);

        _parent.changePage(so);
         QTextStream cout(stdout);
             cout << "change to Phone Pay screen";
     }

     void StateOperations::onLButton_3(){}
     void StateOperations::onLButton_4()
     {
         doCancel();
     }
     void StateOperations::onRButton_1()
     {
         Screen *vo = new ViewShowBalance();
         State *so = new StateShowBalance(*vo,_parent);

        _parent.changePage(so);
         QTextStream cout(stdout);
             cout << "change to Cash Paynent screen";

     }
void StateOperations::onRButton_2()
{
    Screen *vo = new ViewCashPayment();
    State *so = new StateCashPayment(*vo,_parent);

   _parent.changePage(so);
    QTextStream cout(stdout);
        cout << "change to Cash Paynent screen";
}
     void StateOperations::onRButton_3(){}
     void StateOperations::onRButton_4(){}





