#ifndef STATE
#define STATE
#include "atmgui.h"
#include "views/screen.h"
#include <QtCore/QCoreApplication>
class ATMgui;

class State
{
    Screen& _screen;
    ATMgui& _parent;
public:
    State(Screen& scr, ATMgui& parent):_screen(scr),_parent(parent){}
   virtual ~State(){}

   virtual const Screen& screen() const = 0;
   virtual Screen& screen() = 0;

   virtual void onButton_1()=0;

   virtual void onButton_2()=0;

   virtual void onButton_3()=0;

   virtual void onButton_4()=0;

   virtual void onButton_5()=0;

   virtual void onButton_6()=0;

   virtual void onButton_7()=0;

   virtual void onButton_8()=0;

   virtual void onButton_9()=0;

   virtual void onButton_0()=0;

   virtual void onButton_Edit()=0;
   virtual void onButton_Delete()=0;
   virtual void onButton_Cancel()=0;

   virtual void onLButton_1()=0;
   virtual void onLButton_2()=0;
   virtual void onLButton_3()=0;
   virtual void onLButton_4()=0;
   virtual void onRButton_1()=0;
   virtual void onRButton_2()=0;
   virtual void onRButton_3()=0;
   virtual void onRButton_4()=0;

};

#endif // STATE

