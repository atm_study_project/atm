#include <QtCore/QCoreApplication>
#include <QTextStream>
#include "states/stateoperations.h"
#include "states/state.h"
#include "views/screen.h"
#include "views/viewpin.h"
#include "views/viewoperations.h"
#include "views/viewfinishcp.h"
#include "views/viewwelcome.h"

#include "states/statefinishcp.h"
#include "states/statepin.h"
#include "states/statewelcome.h"

void StateFinishCP::doDelete()
{
    _screen.setEditField("");
}

     void StateFinishCP::doEdit()
     {
         QString currStr = _screen.getEditField();
         _screen.setEditField(currStr.left(currStr.size()- 1));
     }

     void StateFinishCP::doCancel()
     {
          Screen *vp = new ViewWelcome();
          State *sp = new StateWelcome(*vp,_parent);
          _parent.changePage(sp);

          // to do return card
          _parent.returnCard();
     }

     void StateFinishCP::input(const QString& str)
     {
         _screen.setEditField(_screen.getEditField()+str);
     }


    StateFinishCP::StateFinishCP(Screen &scr, ATMgui& parent):
        State(scr,parent), _screen(scr), _parent(parent){}
    StateFinishCP::~StateFinishCP(){}

     const Screen& StateFinishCP::screen() const {return _screen;}
     Screen& StateFinishCP::screen() {return _screen;}

     void StateFinishCP::onButton_1(){input("1");}

     void StateFinishCP::onButton_2(){input("2");}

     void StateFinishCP::onButton_3(){input("3");}

     void StateFinishCP::onButton_4(){input("4");}

     void StateFinishCP::onButton_5(){input("5");}

     void StateFinishCP::onButton_6(){input("6");}

     void StateFinishCP::onButton_7(){input("7");}

     void StateFinishCP::onButton_8(){input("8");}

     void StateFinishCP::onButton_9(){input("9");}

     void StateFinishCP::onButton_0(){input("0");}

    //Delete last character in editable field
     void StateFinishCP::onButton_Edit(){doEdit();}
    //Clearing text in editable field on Screen
     void StateFinishCP::onButton_Delete(){doDelete();}
    //Cancelling and returning to Screen with PIN code field
     void StateFinishCP::onButton_Cancel(){doCancel();}

     void StateFinishCP::onLButton_1(){}
     void StateFinishCP::onLButton_2(){}
     void StateFinishCP::onLButton_3(){}
     void StateFinishCP::onLButton_4(){}

     void StateFinishCP::onRButton_1(){}
     void StateFinishCP::onRButton_2(){}
     void StateFinishCP::onRButton_3()
     {
         //button: CANCEL
         // go to main menu

         Screen *vo = new ViewOperations();
         State *so = new StateOperations(*vo,_parent);

        _parent.changePage(so);
        _parent.printReceipt();

         QTextStream cout(stdout);
         cout << "change to Operations screen";
     }
     void StateFinishCP::onRButton_4(){}

