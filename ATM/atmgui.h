#ifndef ATMGUI_H
#define ATMGUI_H

#include <QMainWindow>
#include "states/state.h"
#include "atm/atm.h"

namespace Ui {
class ATMgui;
}

class State;
class ATMgui : public QMainWindow
{
    Q_OBJECT

public:
    explicit ATMgui(QWidget *parent = 0);
    ~ATMgui();
    void changePage(State*&);
    void printReceipt();
    ATM & getATM();
    void returnCard();
    void dispenceCash(Cash & cash);

private slots:
    /*Screen keyboard*/
    void on_leftButton_1_clicked();
    void on_leftButton_2_clicked();
    void on_leftButton_3_clicked();
    void on_leftButton_4_clicked();

    void on_rightButton_1_clicked();
    void on_rightButton_2_clicked();
    void on_rightButton_3_clicked();
    void on_rightButton_4_clicked();
/*Numeric Keyboard*/
    void on_key1_clicked();
    void on_key2_clicked();
    void on_key3_clicked();
    void on_key4_clicked();
    void on_key5_clicked();
    void on_key6_clicked();
    void on_key7_clicked();
    void on_key8_clicked();
    void on_key9_clicked();
    void on_key0_clicked();
  //  void on_delete_clicked();
    void on_cancel_clicked();
    void on_edit_clicked();

  //  void on_pushButton_clicked();

    void on_delete_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void initATM();

    void hideReceipt();

    void toATM();

    void on_dispenser_clicked();

private:
    Ui::ATMgui *ui;
    State *_state;
    ATM _atm;
};

#endif // ATMGUI_H
