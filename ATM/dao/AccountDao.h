#include "dao.h"

class Account;

#ifndef _ABSTRACT_ACCOUNT_DAO_
#define _ABSTRACT_ACCOUNT_DAO_

#include "atm/Card.h"

class AbstractDaoAccount : public virtual Dao<Account, size_t>
{
private:
    virtual Account * const doReadByCard(const Card&) const {return NULL;}

    AbstractDaoAccount(const AbstractDaoAccount&);
    AbstractDaoAccount & operator=(const AbstractDaoAccount&);

protected:
    AbstractDaoAccount():Dao() {return;}

public:
    Account * const readByCard(const Card& card) const {return doReadByCard(card);}
};

#endif


#ifndef _ACCOUNT_DAO_H_
#define _ACCOUNT_DAO_H_

#include <QList>
#include "QSharedPointer"
#include "QSqlQuery"

class DaoAccount final: public virtual AbstractDaoAccount
{
private:

    class SimpleAccountMapper: public virtual AbstractMapper<Account, QSqlQuery>
    {
    private:

        virtual QList<Account> * const doMap(QSqlQuery &) const;

    public:
        SimpleAccountMapper();
        SimpleAccountMapper(const SimpleAccountMapper&);
        SimpleAccountMapper & operator=(const SimpleAccountMapper&);

        virtual ~SimpleAccountMapper();
    };

    QSharedPointer<QSqlDatabase> _connection;
    SimpleAccountMapper _simpleMapper;

    // returns single record from database
    // which is identified by id
    virtual Account * const doRead(const size_t&) const;

    virtual Account * const doReadByCard(const Card&) const;

    virtual void doUpdate(const Account&) const;

public:

    static const QString readQuery;
    static const QString updateQuery;

    explicit DaoAccount(const QSharedPointer<QSqlDatabase>&);
    DaoAccount(const DaoAccount&);
    virtual ~DaoAccount();

    DaoAccount & operator=(const DaoAccount&);

    inline const QSharedPointer<QSqlDatabase> & connection() const {return _connection;}
};

#endif
