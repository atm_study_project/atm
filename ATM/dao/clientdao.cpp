#include "atm/client.h"
#include "clientdao.h"
#include "Dao.h"
#include <QString>
#include "QSqlRecord"
#include "QVariant"
#include "QSqlError"

const QString Client::tableName("client");
const QString Client::idField("client_id");
const QString Client::firstNameField("first_name");
const QString Client::lastNameField("last_name");
const QString Client::addressField("address");

const QString DaoClient::readQuery("SELECT * FROM `" + Client::tableName + "` WHERE `" + Client::idField + "`=:id");

DaoClient::SimpleClientMapper::SimpleClientMapper()
{
    return;
}

DaoClient::SimpleClientMapper::SimpleClientMapper(const SimpleClientMapper&)
{
    return;
}

DaoClient::SimpleClientMapper & DaoClient::SimpleClientMapper::operator=(const DaoClient::SimpleClientMapper&)
{
    return *this;
}

DaoClient::SimpleClientMapper::~SimpleClientMapper()
{
    return;
}

QList<Client> * const DaoClient::SimpleClientMapper::doMap(QSqlQuery & sql) const
{
    QList<Client> * const clients = new QList<Client>();

    while(sql.next())
    {
        Client client;
        QSqlRecord record = sql.record();

        for(int i(0); i < record.count(); ++i)
        {
            if(record.fieldName(i) == Client::idField)
            {
                client.id() = sql.value(i).toInt();
            }
            else if(record.fieldName(i) == Client::firstNameField)
            {
                client.firstName() = sql.value(i).toString();
            }
            else if(record.fieldName(i) == Client::lastNameField)
            {
                client.lastName() = sql.value(i).toString();
            }
            else if(record.fieldName(i) == Client::addressField)
            {
                client.adress() = sql.value(i).toString();
            }
        }
        clients->append(client);
    }

    return clients;
}

DaoClient::DaoClient(const QSharedPointer<QSqlDatabase>& connection):
    Dao(), _connection(connection),
    _simpleMapper()
{
    return;
}

DaoClient::DaoClient(const DaoClient& another):
    Dao(), _connection(another._connection),
    _simpleMapper(another._simpleMapper)
{
    return;
}

DaoClient & DaoClient::operator=(const DaoClient& another)
{
    DaoClient temp(another);

    std::swap(_connection, temp._connection);
    std::swap(_simpleMapper, temp._simpleMapper);

    return *this;
}

DaoClient::~DaoClient()
{
    return;
}

// returns single record from database
// which is identified by id
Client * const DaoClient::doRead(const size_t& id) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoClient::readQuery);
    query.bindValue(":id", id);
    QList<Client> * result(0);

    if(query.exec())
    {
        result = _simpleMapper.map(query);
    }
    else
    {
        delete result;
        throw DaoClient::MysqlException(query.lastError().databaseText());
    }

    if(result->size() > 1)
    {
        delete result;
        throw DaoClient::MysqlException("Not unique record");
    }

    return result->isEmpty() ? NULL : &result->first();
}
