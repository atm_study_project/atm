#include "atm/card.h"
#include "carddao.h"
#include "QSqlRecord"
#include "QVariant"
#include "QSqlError"

const QString Card::tableName("card");
const QString Card::idField("number");
const QString Card::accountIdField("account_id");
const QString Card::expDateField("exp_date");
const QString Card::cvv2Field("cvv2");
const QString Card::pinField("pin");

const QString DaoCard::readQuery("SELECT * FROM `"
                        + Card::tableName +
                        "` WHERE `"
                        + Card::idField + "`=:id");

DaoCard::SimpleCardMapper::SimpleCardMapper()
{
    return;
}

DaoCard::SimpleCardMapper::~SimpleCardMapper()
{
    return;
}

DaoCard::SimpleCardMapper::SimpleCardMapper(const SimpleCardMapper&):
    AbstractMapper()
{
    return;
}

DaoCard::SimpleCardMapper & DaoCard::SimpleCardMapper::operator=(const SimpleCardMapper&)
{
    return *this;
}

QList<Card> * const DaoCard::SimpleCardMapper::doMap(QSqlQuery & sql) const
{
    QList<Card> * const cards = new QList<Card>();

    while(sql.next())
    {
        Card card;
        QSqlRecord record = sql.record();

        for(int i(0); i < record.count(); ++i)
        {
            if(record.fieldName(i) == Card::idField)
            {
                card.number() = sql.value(i).toInt();
            }
            else if(record.fieldName(i) == Card::accountIdField)
            {
                Account account;
                account.id() = sql.value(i).toInt();
                card.account() = account;
            }
            else if(record.fieldName(i) == Card::expDateField)
            {
                card.expDate() = sql.value(i).toLongLong();
            }
            else if(record.fieldName(i) == Card::cvv2Field)
            {
                card.cvv2() = sql.value(i).toInt();
            }
            else if(record.fieldName(i) == Card::pinField)
            {
                card.pin() = sql.value(i).toInt();
            }
        }
        cards->append(card);
    }

    return cards;
}

DaoCard::DaoCard(const QSharedPointer<QSqlDatabase>& connection):
    Dao(), _connection(connection),
    _simpleMapper()
{
    return;
}

DaoCard::DaoCard(const DaoCard & another):
    Dao(), _connection(another._connection),
    _simpleMapper(another._simpleMapper)
{
    return;
}

DaoCard & DaoCard::operator=(const DaoCard& another)
{
    DaoCard temp(another);

    std::swap(_connection, temp._connection);
    std::swap(_simpleMapper, temp._simpleMapper);

    return *this;
}

DaoCard::~DaoCard()
{
    return;
}

// returns single record from database
// which is identified by id
Card * const DaoCard::doRead(const u_long& id) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoCard::readQuery);
    query.bindValue(":id", id);
    QList<Card> * result(0);

    if(query.exec())
    {
        result = _simpleMapper.map(query);
    }
    else
    {
        delete result;
        throw DaoCard::MysqlException(query.lastError().databaseText());
    }

    if(result->size() > 1)
    {
        delete result;
        throw DaoCard::MysqlException("Not unique record");
    }

    return result->isEmpty() ? NULL : &result->first();
}
