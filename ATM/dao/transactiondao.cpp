#include "atm/transaction.h"
#include "transactiondao.h"
#include "QSqlRecord"
#include "QVariant"
#include "QSqlError"

const QString Transaction::tableName("transaction");
const QString Transaction::idField("transaction_id");
const QString Transaction::dateField("date");
const QString Transaction::amountField("amount");
const QString Transaction::commissionField("commission");
const QString Transaction::descriptionField("description");
const QString Transaction::cardNumField("card_number");

const QString DaoTransaction::readQuery
("SELECT * FROM `"
 + Transaction::tableName +
 "` WHERE `"
 + Transaction::idField + "`=:id");

const QString DaoTransaction::createQuery
("INSERT INTO `"
 + Transaction::tableName +
 "` SET `"
 + Transaction::dateField +
 "`=:date,`"
 + Transaction::amountField +
 "`=:amount,`"
 + Transaction::commissionField +
 "`=:commission,`"
 + Transaction::descriptionField +
 "`=:description,`"
 + Transaction::cardNumField +
 "`=:card");

const QString DaoTransaction::destroyQuery
("DELETE FROM `"
 + Transaction::tableName +
 "` WHERE `"
 + Transaction::idField +
 "`=:id");

const QString DaoTransaction::updateQuery
("UPDATE `"
 + Transaction::tableName +
 "` SET `"
 + Transaction::dateField +
 "`=:date,`"
 + Transaction::amountField +
 "`=:amount,`"
 + Transaction::commissionField +
 "`=:commission,`"
 + Transaction::descriptionField +
 "`=:description,`"
 + Transaction::cardNumField +
 "`=:card WHERE `"
 + Transaction::idField +
 "`=:id LIMIT 1"
 );

const QString DaoTransaction::selectAllQuery
("SELECT * FROM `"
 + Transaction::tableName +
 '`');

DaoTransaction::SimpleTransactionMapper::SimpleTransactionMapper()
{
    return;
}

DaoTransaction::SimpleTransactionMapper::~SimpleTransactionMapper()
{
    return;
}

DaoTransaction::SimpleTransactionMapper::SimpleTransactionMapper(const SimpleTransactionMapper&):
    AbstractMapper()
{
    return;
}

DaoTransaction::SimpleTransactionMapper & DaoTransaction::SimpleTransactionMapper::operator=(const SimpleTransactionMapper&)
{
    return *this;
}

QList<Transaction> * const DaoTransaction::SimpleTransactionMapper::doMap(QSqlQuery & sql) const
{
    QList<Transaction> * const transactions = new QList<Transaction>();

    while(sql.next())
    {
        Transaction transaction;
        QSqlRecord record = sql.record();

        for(int i(0); i < record.count(); ++i)
        {
            if(record.fieldName(i) == Transaction::idField)
            {
                transaction.id() = sql.value(i).toInt();
            }
            else if(record.fieldName(i) == Transaction::dateField)
            {
                transaction.date() = sql.value(i).toLongLong();
            }
            else if(record.fieldName(i) == Transaction::amountField)
            {
                transaction.amount() = sql.value(i).toDouble();
            }
            else if(record.fieldName(i) == Transaction::commissionField)
            {
                transaction.commission() = sql.value(i).toDouble();
            }
            else if(record.fieldName(i) == Transaction::descriptionField)
            {
                transaction.description() = sql.value(i).toString();
            }
            else if(record.fieldName(i) == Transaction::cardNumField)
            {
                Card card;
                card.number() = sql.value(i).toLongLong();
                transaction.card() = card;
            }
        }
        transactions->append(transaction);
    }

    return transactions;
}

DaoTransaction::DaoTransaction(const QSharedPointer<QSqlDatabase>& connection):
    Dao(), _connection(connection),
    _simpleMapper()
{
    return;
}

DaoTransaction::DaoTransaction(const DaoTransaction& another):
    Dao(), _connection(another._connection),
    _simpleMapper(another._simpleMapper)
{
    return;
}

DaoTransaction & DaoTransaction::operator=(const DaoTransaction& another)
{
    DaoTransaction temp(another);

    std::swap(_connection, temp._connection);
    std::swap(_simpleMapper, temp._simpleMapper);

    return *this;
}

DaoTransaction::~DaoTransaction()
{
    return;
}

// returns single record from database
// which is identified by id
Transaction * const DaoTransaction::doRead(const size_t& id) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoTransaction::readQuery);
    query.bindValue(":id", id);
    QList<Transaction> * result(0);

    if(query.exec())
    {
        result = _simpleMapper.map(query);
    }
    else
    {
        delete result;
        throw DaoTransaction::MysqlException(query.lastError().databaseText());
    }

    if(result->size() > 1)
    {
        delete result;
        throw DaoTransaction::MysqlException("Not unique record");
    }

    return result->isEmpty() ? NULL : &result->first();
}

void DaoTransaction::doCreate(const Transaction& transaction) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoTransaction::createQuery);
    query.bindValue(":date", transaction.date());
    query.bindValue(":amount", transaction.amount());
    query.bindValue(":commission", transaction.commission());
    query.bindValue(":description", transaction.description());
    query.bindValue(":card", transaction.card().number());

    if(!query.exec())
    {
        throw DaoTransaction::MysqlException(query.lastError().databaseText());
    }
}

void DaoTransaction::doDestroy(const size_t& id) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoTransaction::destroyQuery);
    query.bindValue(":id", id);

    if(!query.exec())
    {
        throw DaoTransaction::MysqlException(query.lastError().databaseText());
    }
}

// updates given entity in database
void DaoTransaction::doUpdate(const Transaction& transaction) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoTransaction::updateQuery);
    query.bindValue(":date", transaction.date());
    query.bindValue(":amount", transaction.amount());
    query.bindValue(":commission", transaction.commission());
    query.bindValue(":description", transaction.description());
    query.bindValue(":card", transaction.card().number());
    query.bindValue(":id", transaction.id());

    if(!query.exec())
    {
        throw DaoTransaction::MysqlException(query.lastError().databaseText());
    }
}

// returns list of all entities from database
QList<Transaction> * const DaoTransaction::doAll() const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoTransaction::selectAllQuery);

    QList<Transaction> *result(0);

    if(query.exec())
    {
        result = _simpleMapper.map(query);
    }
    else
    {
        delete result;
        throw DaoTransaction::MysqlException(query.lastError().databaseText());
    }

    return result->isEmpty() ? NULL : result;
}
