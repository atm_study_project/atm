#ifndef _CLIENT_DAO_H_
#define _CLIENT_DAO_H_

#include <QList>
#include "dao.h"
#include "QSharedPointer"
#include "QSqlQuery"

class Client;

class DaoClient final: public virtual Dao<Client, size_t>
{
private:

    class SimpleClientMapper: public virtual AbstractMapper<Client, QSqlQuery>
    {
    private:

        virtual QList<Client> * const doMap(QSqlQuery &) const;

    public:
        SimpleClientMapper();
        SimpleClientMapper(const SimpleClientMapper&);
        SimpleClientMapper & operator=(const SimpleClientMapper&);

        virtual ~SimpleClientMapper();
    };

    QSharedPointer<QSqlDatabase> _connection;
    SimpleClientMapper _simpleMapper;

    // returns single record from database
    // which is identified by id
    virtual Client * const doRead(const size_t&) const;

public:

    static const QString readQuery;

    explicit DaoClient(const QSharedPointer<QSqlDatabase>&);
    DaoClient(const DaoClient&);
    virtual ~DaoClient();

    DaoClient & operator=(const DaoClient&);

    inline const QSharedPointer<QSqlDatabase> & connection() const {return _connection;}
};

#endif
