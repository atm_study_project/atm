#ifndef _CARD_DAO_H_
#define _CARD_DAO_H_

#include <QList>
#include "dao.h"
#include "QSharedPointer"
#include "QSqlQuery"

typedef unsigned long long u_long;
class Card;

class DaoCard final: public virtual Dao<Card, u_long>
{
private:

    // don't need final keyword here
    class SimpleCardMapper: public virtual AbstractMapper<Card, QSqlQuery>
    {
    private:
        virtual QList<Card> * const doMap(QSqlQuery &) const;

    public:
        SimpleCardMapper();
        SimpleCardMapper(const SimpleCardMapper&);
        virtual ~SimpleCardMapper();

        SimpleCardMapper & operator=(const SimpleCardMapper&);
    };

    QSharedPointer<QSqlDatabase> _connection;
    SimpleCardMapper _simpleMapper;

    // returns single record from database
    // which is identified by id
    virtual Card * const doRead(const u_long& number) const;

public:

    static const QString readQuery;

    explicit DaoCard(const QSharedPointer<QSqlDatabase>&);
    DaoCard(const DaoCard&another);
    virtual ~DaoCard();

    DaoCard & operator=(const DaoCard&);

    inline const QSharedPointer<QSqlDatabase> & connection() const {return _connection;}
};

#endif
