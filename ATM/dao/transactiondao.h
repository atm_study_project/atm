#ifndef _TRANSACTION_DAO_H_
#define _TRANSACTION_DAO_H_

#include <QList>
#include "dao.h"
#include "QSharedPointer"
#include "QSqlQuery"

class Transaction;

// Terminal class
class DaoTransaction final: public virtual Dao<Transaction, size_t>
{
private:

    class SimpleTransactionMapper: public virtual AbstractMapper<Transaction, QSqlQuery>
    {
    private:

        virtual QList<Transaction> * const doMap(QSqlQuery &) const;

    public:
        SimpleTransactionMapper();
        SimpleTransactionMapper(const SimpleTransactionMapper&);

        virtual ~SimpleTransactionMapper();

        SimpleTransactionMapper & operator=(const SimpleTransactionMapper&);
    };

    QSharedPointer<QSqlDatabase> _connection;
    SimpleTransactionMapper _simpleMapper;

    // returns single record from database
    // which is identified by id
    virtual Transaction * const doRead(const size_t&) const;

    // These methods should not be implemented
    virtual void doCreate(const Transaction&) const;

    // removes entry from database
    virtual void doDestroy(const size_t&) const;

    // updates given entity in database
    virtual void doUpdate(const Transaction&) const;

    // returns list of all entities from database
    virtual QList<Transaction> * const doAll() const;

public:

    static const QString readQuery;
    static const QString createQuery;
    static const QString destroyQuery;
    static const QString updateQuery;
    static const QString selectAllQuery;

    explicit DaoTransaction(const QSharedPointer<QSqlDatabase>&);
    DaoTransaction(const DaoTransaction&);
    virtual ~DaoTransaction();

    DaoTransaction & operator=(const DaoTransaction&);

    inline const QSharedPointer<QSqlDatabase> & connection() const {return _connection;}
};

#endif
