#ifndef _DAO_H_
#define _DAO_H_

#include <cstddef>

#include <QList>
#include <QString>

/*
 * This class represents
 * absract interface
 * for sql row mapper;
 * E - entity type
 * RS - sql result set type
*/

template <class E, class RS>
class AbstractMapper
{
private:
    virtual QList<E> * const doMap(RS &) const = 0;
    //  Abstracr class doesn't have realization
    //  that's why it can't have assign operator
    //  and copy-constructor
    AbstractMapper & operator=(const AbstractMapper&);
    AbstractMapper(const AbstractMapper&);

protected:
    AbstractMapper();

public:
    virtual ~AbstractMapper();
    QList<E> * const map(RS& rs) const {return doMap(rs);}
};

/*
This class represents
abstract interface
for DAO;
E represents entity class
*/
template <class E, typename I = size_t>
class Dao
{
private:

    // because abstract class
    Dao & operator=(const Dao&);
    Dao(const Dao&);

    // default behavior, should be redefined in
    // inhereted classes
    virtual QList<E> * const doAll() const {return NULL;}
    virtual void doCreate(const E&) const {}
    virtual E * const doRead(const I&) const {return NULL;}
    virtual void doUpdate(const E&) const {}
    virtual void doDestroy(const I&) const {}

protected:
    Dao();

public:

    // This class rerpesents
    // errors which may occure
    // while processing db request/responce
    class MysqlException;

    virtual ~Dao();

    // returns list of all entities from database
    QList<E> * const all() const {return doAll();}

    // creates a new record in database
    void create(const E& e) const {doCreate(e);}

    // returns single record from database
    // which is identified by id
    E * const read(const I& id) const {return doRead(id);}

    // updates given entity in database
    void update(const E& e) const {return doUpdate(e);}

    // destroys given entity which is identified
    // by id
    void destroy(const I& id) const {doDestroy(id);}

};

template <class E, class RS>
AbstractMapper<E,RS>::AbstractMapper()
{
    return;
}

template <class E, class RS>
AbstractMapper<E,RS>::AbstractMapper(const AbstractMapper<E,RS>&)
{
    return;
}

template <class E, class RS>
AbstractMapper<E,RS>::~AbstractMapper()
{
    return;
}

template <class E, typename I>
class Dao<E, I>::MysqlException
{
private:
    const QString _info;

public:
    MysqlException(const QString& info);
    ~MysqlException();

    const QString & info() const;
};

template <class E, typename I>
Dao<E, I>::Dao()
{
    return;
}

template <class E, typename I>
Dao<E, I>::~Dao()
{
    return;
}

template <class E, typename I>
Dao<E, I>::Dao(const Dao<E, I>&)
{
    return;
}

template <class E, typename I>
Dao<E, I>::MysqlException::MysqlException(const QString& info):
    _info(info)
{
    return;
}

template <class E, typename I>
Dao<E, I>::MysqlException::~MysqlException()
{
    return;
}

template <class E, typename I>
const QString & Dao<E, I>::MysqlException::info() const
{
    return _info;
}

#endif
