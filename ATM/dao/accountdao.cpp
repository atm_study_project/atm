#include "atm/Account.h"
#include "dao/accountdao.h"
#include <QSqlRecord>
#include "QVariant"
#include <QSqlError>

const QString Account::tableName("account");
const QString Account::idField("account_id");
const QString Account::clientIdField("client_id");
const QString Account::balanceField("balance");
const QString Account::typeField("type");

const QString DaoAccount::readQuery(
"SELECT * FROM `"
+ Account::tableName +
"` WHERE `"
+ Account::idField + "`=:id");

const QString DaoAccount::updateQuery
("UPDATE `"
 + Account::tableName +
 "` SET `"
 + Account::balanceField +
 "`=:balance,`"
 + Account::typeField +
 "`=:type WHERE `"
 + Account::idField +
 "`=:id LIMIT 1"
 );

DaoAccount::SimpleAccountMapper::SimpleAccountMapper()
{
    return;
}

DaoAccount::SimpleAccountMapper::SimpleAccountMapper(const SimpleAccountMapper&):
    AbstractMapper()
{
    return;
}

DaoAccount::SimpleAccountMapper & DaoAccount::SimpleAccountMapper::operator=(const SimpleAccountMapper&)
{
    return *this;
}

DaoAccount::SimpleAccountMapper::~SimpleAccountMapper()
{
    return;
}

QList<Account> * const DaoAccount::SimpleAccountMapper::doMap(QSqlQuery & sql) const
{
    QList<Account> * const accounts = new QList<Account>();

    while(sql.next())
    {
        Account account;
        QSqlRecord record = sql.record();

        for(int i(0); i < record.count(); ++i)
        {
            if(record.fieldName(i) == Account::idField)
            {
                account.id() = sql.value(i).toInt();
            }
            else if(record.fieldName(i) == Account::clientIdField)
            {
                Client client;
                client.id() = sql.value(i).toInt();
                account.client() = client;
            }
            else if(record.fieldName(i) == Account::balanceField)
            {
                account.balance() = sql.value(i).toDouble();
            }
            else if(record.fieldName(i) == Account::typeField)
            {
                account.type() = Account::AccountType(sql.value(i).toInt());
            }
        }
        accounts->append(account);
    }

    return accounts;
}

DaoAccount::DaoAccount(const QSharedPointer<QSqlDatabase>& connection):
    AbstractDaoAccount(), _connection(connection),
    _simpleMapper()
{
    return;
}

DaoAccount::DaoAccount(const DaoAccount & another):
    AbstractDaoAccount(), _connection(another._connection),
    _simpleMapper(another._simpleMapper)
{
    return;
}

DaoAccount & DaoAccount::operator=(const DaoAccount& another)
{
    DaoAccount temp(another);

    std::swap(_connection, temp._connection);
    std::swap(_simpleMapper, temp._simpleMapper);

    return *this;
}

DaoAccount::~DaoAccount()
{
    return;
}

Account * const DaoAccount::doReadByCard(const Card& card) const
{
    return doRead(card.account().id());
}

// returns single record from database
// which is identified by id
Account * const DaoAccount::doRead(const size_t& id) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoAccount::readQuery);
    query.bindValue(":id", id);
    QList<Account> * result(0);

    if(query.exec())
    {
        result = _simpleMapper.map(query);
    }
    else
    {
        delete result;
        throw DaoAccount::MysqlException(query.lastError().databaseText());
    }

    if(result->size() > 1)
    {
        delete result;
        throw DaoAccount::MysqlException("Not unique record");
    }

    return result->isEmpty() ? NULL : &result->first();
}

void DaoAccount::doUpdate(const Account& account) const
{
    QSqlQuery query(*connection().data());
    query.prepare(DaoAccount::updateQuery);
    query.bindValue(":balance", account.balance());
    query.bindValue(":type", account.type());
    query.bindValue(":id", account.id());

    if(!query.exec())
    {
        throw DaoAccount::MysqlException(query.lastError().databaseText());
    }
}
