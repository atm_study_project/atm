#include "atmgui.h"
#include <QApplication>
#include <QtCore/QCoreApplication>
#include <QtSQL>

#include "atm/atm.h"
#include "atm/creditcard.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ATMgui w;

    w.show();

    return a.exec();
}
