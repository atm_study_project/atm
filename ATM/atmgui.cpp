#include "atmgui.h"
#include "ui_atmgui.h"

#include <QTextEdit>
#include "states/statewelcome.h"
#include "views/viewwelcome.h"
#include "states/statepin.h"
#include "views/viewpin.h"
#include "atm/creditcard.h"
#include "atm/receipt.h"
#include "states/stateadminatm.h"
#include "views/viewadminatm.h"
#include <QPushButton>
#include <QSpinBox>

#include <QWidget>
#include <iostream>
using namespace std;

ATMgui::ATMgui(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ATMgui),
    _state(0)
{
    Screen *_screen = new ViewWelcome();
    _state = new StateWelcome(*_screen, *this);

    ui->setupUi(this);
    ui->receipt->setVisible(false);

    changePage(_state);
}

ATM & ATMgui::getATM()
{
    return _atm;
}

void ATMgui::returnCard()
{
    ui->groupBox->setVisible(true);

    ui->pushButton->setDisabled(false);
    delete _atm.returnCard();

    ui->card->setText("");
    return;
}

void ATMgui::changePage(State *&state)
{
   delete ui->view->currentWidget();
   _state = state;

   ui->view->addWidget(&state->screen());
   ui->view->setCurrentWidget(&state->screen());
}

ATMgui::~ATMgui()
{
    delete ui;
}

void ATMgui::initATM()
{
    int grn50  = ui->centralwidget->findChild<QSpinBox *>("spin50grn")->text().toInt();
    int grn100 = ui->centralwidget->findChild<QSpinBox *>("spin100grn")->text().toInt();
    int grn200 = ui->centralwidget->findChild<QSpinBox *>("spin200grn")->text().toInt();
    int grn500 = ui->centralwidget->findChild<QSpinBox *>("spin500grn")->text().toInt();
    _atm.refillCash(Cash(grn50, grn100, grn200, grn500));

   /* ui->centralwidget->findChild<QLabel *>("balance")->setText(QString::number(grn50*50
                                                                       +grn100*100
                                                                       +grn200*200
                                                                       +grn500*500
                                                                       )
                                                               + " UAH");*/

    ui->centralwidget->findChild<QLabel *>("balance")->setText(QString::number(_atm.currentCash().sum())
                                                               + " UAH");

}

void ATMgui::on_leftButton_1_clicked()
{
    _state->onLButton_1();
}

void ATMgui::on_leftButton_2_clicked()
{
    _state->onLButton_2();
}

void ATMgui::on_leftButton_3_clicked()
{
    _state->onLButton_3();
}

void ATMgui::on_leftButton_4_clicked()
{
    _state->onLButton_4();
}

void ATMgui::on_rightButton_1_clicked()
{
    _state->onRButton_1();
}

void ATMgui::on_rightButton_2_clicked()
{
    _state->onRButton_2();
}

void ATMgui::on_rightButton_3_clicked()
{
    _state->onRButton_3();
}

void ATMgui::on_rightButton_4_clicked()
{
    _state->onRButton_4();
}

void ATMgui::on_key0_clicked()
{
    _state->onButton_0();
}

void ATMgui::on_key1_clicked()
{
    _state->onButton_1();
}

void ATMgui::on_key2_clicked()
{
    _state->onButton_2();
}

void ATMgui::on_key3_clicked()
{
    _state->onButton_3();
}

void ATMgui::on_key4_clicked()
{
    _state->onButton_4();
}

void ATMgui::on_key5_clicked()
{
    _state->onButton_5();
}

void ATMgui::on_key6_clicked()
{
    _state->onButton_6();
}
void ATMgui::on_key7_clicked()
{
    _state->onButton_7();
}

void ATMgui::on_key8_clicked()
{
    _state->onButton_8();
}

void ATMgui::on_key9_clicked()
{
    _state->onButton_9();
}

void ATMgui::on_cancel_clicked()
{
    _state->onButton_Cancel();
}
void ATMgui::on_edit_clicked()
{
    _state->onButton_Edit();
}

void ATMgui::on_delete_2_clicked()
{
    _state->onButton_Delete();
}

void ATMgui::hideReceipt()
{
    if(ui->receipt->isVisible())
    {
        ui->receipt->setVisible(false);
    }
    return;
}

void ATMgui::printReceipt()
{
    if(!ui->receipt->isVisible() && _atm.hasReceipt()){

    Receipt receipt(_atm.getReceipt());
    ui->receipt->setVisible(true);
    ui->receipt->setText("<center>"
                         +receipt._title
                         +"<br>"
                          "-------------"
                          "<br>"
                          "</center>"
                         + "<b>"
                         + receipt._description
                         + "</b>"
                         + "<br>"
                         + "<b>AMOUNT: </b>"
                         + "Amount: " + QString::number(receipt._amount, 'f', 2)
                         + " UAH <br>"
                         + "Commission: " + QString::number(receipt._commission, 'f', 2)
                         + " UAH<br>"
                         + "<br>"

                         + "<center><b>THANK YOU</b></center>"
                         + "<br>"
                         + receipt._date.toString()
                        );
    }

    QTextEdit *receipt = ui->centralwidget->findChild<QTextEdit*>("receipt");
    if(receipt != 0)
    {
        connect(receipt, SIGNAL(cursorPositionChanged()),this, SLOT(hideReceipt()));
    }
    return;
}

void ATMgui::dispenceCash(Cash & cash)
{
    ui->dispenser->setText("Your cash:"+QString::number(cash.sum())+" UAH"
                           + "\n 50 UAH: "+QString::number(cash.getBillCount(0))
                           + "     100 UAH: "+QString::number(cash.getBillCount(1))
                           + "\n200 UAH: "+QString::number(cash.getBillCount(2))
                           + "     500 UAH: "+QString::number(cash.getBillCount(3)));
    ui->dispenser->setStyleSheet("background-color:green;\nborder: 1px dashed black;\ncolor:white;\nfont-size:7pt;");
    return;
}

void ATMgui::on_pushButton_clicked()
{
    Screen *vp = new ViewPIN();
    State *sp = new StatePIN(*vp,*this);

   changePage(sp);
    QTextStream cout(stdout);
        cout << "change to PIN screen";
    QTextEdit *t = ui->groupBox->findChild<QTextEdit *>("card");
    _atm.insertCard(new CreditCard(t->toPlainText().toLongLong()));
    ui->groupBox->setVisible(false);
    ui->pushButton->setDisabled(true);
}

void ATMgui::on_pushButton_2_clicked()
{
    Screen *ac = new ViewAdminATM();
    State *admin = new StateAdminATM(*ac,*this);
    changePage(admin);
    _atm.switchToMaintenance();

   QPushButton *save = ui->centralwidget->findChild<QPushButton *>("save");
   if(save != 0)
   {
       connect(save, SIGNAL(clicked()),this, SLOT(initATM()));
   }

   QPushButton *toATM = ui->centralwidget->findChild<QPushButton *>("toATM");
   if(toATM != 0)
   {
       connect(toATM, SIGNAL(clicked()),this, SLOT(toATM()));
   }
}

void ATMgui::toATM()
{
    returnCard();
    Screen *ac = new ViewWelcome();
    State *welc = new StateWelcome(*ac,*this);
    changePage(welc);
    _atm.switchToActive();
    QTextStream cout(stdout);
        cout << "return to Welcome";
}

void ATMgui::on_dispenser_clicked()
{
    ui->dispenser->setText("SLOT FOR MONEY");
}
