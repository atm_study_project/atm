typedef unsigned long long u_long;

#ifndef _CARD_H_
#define _CARD_H_

#include <QString>
#include <iostream>

#include "account.h"

typedef unsigned long long u_long;

// Entity class
class Card final
{
private:
    u_long _number;
    Account _account;
    u_long _exp_date;
    size_t _cvv2;
    size_t _pin;

public:

    static const QString tableName;
    static const QString idField;
    static const QString accountIdField;
    static const QString expDateField;
    static const QString cvv2Field;
    static const QString pinField;

    Card(
        u_long number=0,
        const Account& account=Account(),
        u_long exp_date=0,
        size_t cvv2=0,
        size_t pin=0
        );
    Card(const Card&);
    ~Card();

    inline const u_long & number() const {return _number;}
    inline const Account & account() const {return _account;}
    inline const u_long & expDate() const {return _exp_date;}
    inline const size_t & cvv2() const {return _cvv2;}
    inline const size_t & pin() const {return _pin;}

    inline u_long & number() {return _number;}
    inline Account & account() {return _account;}
    inline u_long & expDate() {return _exp_date;}
    inline size_t & cvv2() {return _cvv2;}
    inline size_t & pin() {return _pin;}
};

std::ostream& operator<<(std::ostream&, const Card &);

#endif
