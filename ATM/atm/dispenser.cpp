#include "dispenser.h"
#include "atm.h"
#include <cassert>

const unsigned int Dispenser::MAX_AMOUNT = 3000;

Dispenser::Dispenser(const Cash& cash):
    _cash(cash)
{
    return;
}

Dispenser::~Dispenser()
{
    return;
}

Cash Dispenser::currentCash() const
{
    return _cash;
}

void Dispenser::refillCash(const Cash& c) {
    _cash += c;
}

unsigned int Dispenser::minimalBill() const {
    size_t min(0);
    for(size_t i(0); i < Cash::BILLS_COUNT; ++i) {
        if(_cash.getBillCount(i) != 0){
            min = Cash::BILL_VALUES[i];
            break;
        }
    }
    return min;
}

bool Dispenser::canDispense(unsigned int amount) const {
    return (minimalBill() != 0
            && amount % minimalBill() == 0
            && amount <= MAX_AMOUNT
            && amount <= _cash.sum());
}

Cash Dispenser::dispense(unsigned int amount) {
    Cash giveaway;
    unsigned int amountLeft(amount);
    for(int i(Cash::BILLS_COUNT); i >= 0; --i) {
        unsigned int nBills(qMin(amountLeft / Cash::BILL_VALUES[i], _cash.getBillCount(i)));
        amountLeft -= Cash::BILL_VALUES[i] * nBills;
        giveaway.setBillCount(i, nBills);
    }
    assert(giveaway.sum() == amount);
    _cash -= giveaway;
    return giveaway;
}
