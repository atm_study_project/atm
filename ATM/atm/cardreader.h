#ifndef CARDREADER
#define CARDREADER

class CreditCard;

class CardReader
{
private:
    const CreditCard* _currentCard;

public:
    CardReader();
    ~CardReader();

    void insertCard(const CreditCard*);
    bool hasCard() const;

    unsigned long long read() const;
    const CreditCard* returnCard();
};

#endif // CARDREADER

