#ifndef CREDITCARD
#define CREDITCARD

#include <QString>

class CreditCard
{
    friend class CardReader;

private:
    const unsigned long long _number;

    CreditCard(const CreditCard&);
    CreditCard& operator=(const CreditCard&);

public:
    CreditCard(unsigned long long number);
    ~CreditCard();
};

#endif // CREDITCARD

