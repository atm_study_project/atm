#ifndef _TRANSACTION_H_
#define _TRANSACTION_H_

#include <iostream>

#include "card.h"
#include <QString>

class Transaction final
{
public:

    static const QString tableName;
    static const QString idField;
    static const QString dateField;
    static const QString amountField;
    static const QString commissionField;
    static const QString descriptionField;
    static const QString cardNumField;

    Transaction(
            size_t id=0,
            const Card& card=Card(),
            unsigned long long date=0,
            double amount=0,
            double commision=0,
            const QString& description=""
            );
    Transaction(const Transaction&transaction);
    ~Transaction();

    inline const size_t & id() const {return _id;}
    inline const Card & card() const {return _card;}
    inline const unsigned long long & date() const {return _date;}
    inline const double & amount() const {return _amount;}
    inline const double & commission() const {return _commission;}
    inline const QString & description() const {return _description;}

    inline size_t & id() {return _id;}
    inline Card & card() {return _card;}
    inline unsigned long long & date() {return _date;}
    inline double & amount() {return _amount;}
    inline double & commission() {return _commission;}
    inline QString & description() {return _description;}

private:
    size_t _id;
    Card _card;
    unsigned long long _date;
    double _amount;
    double _commission;
    QString _description;
};

std::ostream& operator<<(std::ostream&, const Transaction &);

#endif
