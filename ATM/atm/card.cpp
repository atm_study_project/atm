#include "Card.h"

Card::Card(unsigned long long number, const Account& account, unsigned long long exp_date, size_t cvv2, size_t pin):
    _number(number), _account(account),
    _exp_date(exp_date), _cvv2(cvv2),
    _pin(pin)
{
    return;
}

Card::Card(const Card& card):
    _number(card.number()), _account(card.account()),
    _exp_date(card.expDate()), _cvv2(card.cvv2()),
    _pin(card.pin())
{
    return;
}

Card::~Card()
{
    return;
}

std::ostream& operator<<(std::ostream& os, const Card & card)
{
    os << card.number() << ' ' << card.account().id() << ' ';
    os << card.expDate() << ' ' << card.cvv2() << ' ';
    os << card.pin();
    return os;
}

