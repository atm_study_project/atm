#ifndef PRINTER
#define PRINTER

class ATM;
class Receipt;

class Printer
{
private:
    const Receipt * _currentReceipt;

public:
    Printer();
    ~Printer();

    void print(const Receipt*);
    bool hasReceipt() const;
    Receipt getReceipt();
};
#endif // PRINTER

