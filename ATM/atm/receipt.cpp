#include "receipt.h"

Receipt::Receipt(const QString& title,
                 unsigned long long cardNumber,
                 const QDateTime& date,
                 int amount, double commission,
                 const QString& desc):
    _title(title), _cardNumber(cardNumber),
    _date(date), _amount(amount),
    _commission(commission), _description(desc)
{
    return;
}

Receipt::Receipt(const Receipt& r):
     _title(r._title), _cardNumber(r._cardNumber),
     _date(r._date), _amount(r._amount),
     _commission(r._commission), _description(r._description)
{
    return;
}

Receipt::~Receipt()
{
    return;
}

Receipt& Receipt::operator=(const Receipt& r)
{
    _title = r._title;
    _cardNumber = r._cardNumber;
    _date = r._date;
    _amount = r._amount;
    _commission = r._commission;
    _description = r._description;
    return (*this);
}
