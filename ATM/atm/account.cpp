#include "account.h"

Account::Account
(size_t id, const Client& c, double bal, const AccountType& type):
    _id(id),_client(c), _balance(bal), _type(type)
{
    return;
}

Account::Account(const Account& account):
    _id(account.id()),
    _client(account.client()),
    _balance(account.balance()),
    _type(account.type())
{
    return;
}

Account::~Account()
{
    return;
}

std::ostream& operator<<(std::ostream& os, const Account & acc)
{
    os << acc.id() << ' ' << acc.client() << ' ';
    os << acc.balance() << ' ' << acc.type();
    return os;
}

double Account::COMMISSION[Account::AccountType::TYPES_COUNT] = {0.01, 0.02};

