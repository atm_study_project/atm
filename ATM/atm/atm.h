#ifndef _ATM_
#define _ATM_

#include <QString>

#include "cardreader.h"
#include "dispenser.h"
#include "printer.h"
#include "Account.h"
class Card;

class ATM
{
private:
    class DataSource;
    enum State {
        MAINTENANCE, INITIAL, CARD_INSERTED, AUTHORIZED
    };
    //State to monitor correct ATM workflow
    State _currentState;

    //ATM components
    CardReader _cardReader;
    Dispenser  _dispenser;
    Printer    _printer;

    //Source of persistent data
    DataSource* _dataSource;

    //Restricted methods
    ATM(const ATM&);
    ATM& operator=(const ATM&);

    //Utility methods
    Card* retrieveCard(unsigned long long) const;
    Account* retrieveAccount(const Card&) const;
    double round(double) const;
    double calcCommission(unsigned int, Account::AccountType) const;
    inline double commissionedAmount(unsigned int, Account::AccountType) const;
public:

    class ATMException;

    ATM();
    ~ATM();

    bool isAuthorized() const;
    void switchToMaintenance();
    void switchToActive();

    //CardReader actions
    void insertCard(const CreditCard*);
    const CreditCard* returnCard();

    //Dispenser actions
    void refillCash(const Cash&);
    unsigned int minimalBill() const;
    Cash currentCash() const;

    //Printer actions
    bool hasReceipt() const;
    Receipt getReceipt();

    //ATM optional conditions to check before calling ATM operations
    //If not matched will cause ATMExceptions
    bool validCardAccount() const;
    bool validCardAccount(unsigned long long) const;
    bool balanceExceeds(unsigned int) const;

    bool canDispense(unsigned int) const;

    //ATM operations
    bool checkPin(unsigned int pin);
    double checkBalance();
    Cash withdraw(unsigned int amount);
    void transferToAccount(unsigned long long cardNumber, unsigned int amount);
    void transferToMobile(QString number, unsigned int amount);
};

class ATM::ATMException {
private:
    QString _message;

public:
    ATMException(QString message);
    ~ATMException();
    ATMException(const ATMException&);
    ATMException& operator=(const ATMException&);

    const QString& getMessage() const;
};

#endif // ATM
