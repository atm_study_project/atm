#ifndef CASH_H
#define CASH_H

#include <QString>

class Cash
{
public:
    enum Bill {
        FIFTY=0, HUNDRED, TWO_HUNDRED, FIVE_HUNDRED, BILLS_COUNT
    };

    static const unsigned int BILL_VALUES[BILLS_COUNT];

    Cash(unsigned int fifties=0,
         unsigned int hundreds=0,
         unsigned int twoHundreds=0,
         unsigned int fiveHundreds=0);
    Cash(const Cash&);
    Cash& operator=(const Cash&);
    ~Cash();

    unsigned int getBillCount(Bill) const;
    unsigned int getBillCount(unsigned int) const;
    void setBillCount(Bill, unsigned int);
    void setBillCount(unsigned int, unsigned int);
    unsigned int sum() const;
private:
    unsigned int _bills[BILLS_COUNT];
};

Cash& operator+=(Cash&, const Cash&);
Cash& operator-=(Cash&, const Cash&);

QString toString(const Cash&);

#endif // CASH_H
