#include "DataSource.h"
#include <QDebug>
#include <QSqlError>

ATM::DataSource::DataSource(const QString& connName):
    _connName(connName),
    _conn(new QSqlDatabase()),
    _accountDao(0),
    _cardDao(0),
    _clientDao(0),
    _transactionDao(0)
{
    *_conn = QSqlDatabase::addDatabase( _connName );
    _conn->setHostName( "144.76.71.56" );
    _conn->setDatabaseName( "user0512_atm" );
    _conn->setUserName( "user0512_atm" );
    _conn->setPassword( "user0512_atm" );
    if( !_conn->open() )
    {
      qDebug() << _conn->lastError();
      qFatal( "Failed to connect" );
    }
    qDebug( "Connection opened" );
    return;
}

ATM::DataSource::~DataSource() {
    _conn->close();
    QSqlDatabase::removeDatabase( _connName );
     qDebug() << "Connection closed";
    return;
}

const DaoAccount& ATM::DataSource::accountDao() const {
    if(!_accountDao) {
        _accountDao = new DaoAccount(_conn);
    }
    return *_accountDao;
}

const DaoCard& ATM::DataSource::cardDao() const {
    if(!_cardDao) {
        _cardDao = new DaoCard(_conn);
    }
    return *_cardDao;
}
const DaoClient& ATM::DataSource::clientDao() const {
    if(!_clientDao) {
        _clientDao = new DaoClient(_conn);
    }
    return *_clientDao;
}

const DaoTransaction& ATM::DataSource::transactionDao() const {
    if(!_transactionDao) {
        _transactionDao = new DaoTransaction(_conn);
    }
    return *_transactionDao;
}
