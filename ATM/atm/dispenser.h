#ifndef DISPENSER
#define DISPENSER

#include "cash.h"

class ATM;

class Dispenser
{
private:
    Cash _cash;

public:
    Dispenser(const Cash& cash = Cash());
    ~Dispenser();

    Cash currentCash() const;
    void refillCash(const Cash&);
    unsigned int minimalBill() const;
    bool canDispense(unsigned int amount) const;
    Cash dispense(unsigned int);

    static const unsigned int MAX_AMOUNT;
};


#endif // DISPENSER

