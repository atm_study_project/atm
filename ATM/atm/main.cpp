#include <QtCore/QCoreApplication>
#include <QtSQL>

#include "atm.h"
#include "creditcard.h"

#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    CreditCard * credit = new CreditCard(1);

    ATM atm;
    atm.refillCash(Cash(10, 5, 2));
    atm.insertCard(credit);
    try {
        cout << boolalpha << atm.validPin(3214) << endl;
        cout << atm.checkBalance() << endl;
        int amount(400);
        if (atm.balanceExceeds(amount)) {
            Cash cash = atm.withdraw(amount);
            cout << cash.sum() << endl;
        }
    }
    catch (const ATM::ATMException& ex) {
        cout << ex.getMessage().toStdString() << endl;
    }


    delete credit;
    return a.exec();
}
