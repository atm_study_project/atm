#include "cash.h"

const unsigned int Cash::BILL_VALUES[BILLS_COUNT] = {50, 100, 200, 500};

Cash::Cash(unsigned int fifties,
     unsigned int hundreds,
     unsigned int twoHundreds,
     unsigned int fiveHundreds)
{
    _bills[FIFTY] = fifties;
    _bills[HUNDRED] = hundreds;
    _bills[TWO_HUNDRED] = twoHundreds;
    _bills[FIVE_HUNDRED] = fiveHundreds;
    return;
}

Cash::Cash(const Cash& c)
{
    for(unsigned int i(0); i < BILLS_COUNT; ++i) {
        _bills[i] = c._bills[i];
    }
    return;
}

Cash& Cash::operator=(const Cash& c) {
    for(unsigned int i(0); i < BILLS_COUNT; ++i) {
        _bills[i] += c._bills[i];
    }
    return (*this);
}

Cash::~Cash() {
    return;
}

unsigned int Cash::getBillCount(Bill b) const {
    return _bills[b];
}

unsigned int Cash::getBillCount(unsigned int b) const {
    return _bills[b];
}

void Cash::setBillCount(Bill b, unsigned int val) {
    _bills[b] = val;
}

void Cash::setBillCount(unsigned int b, unsigned int val) {
    _bills[b] = val;
}

unsigned int Cash::sum() const {
    unsigned int sum(0);
    for(unsigned int i = 0; i < BILLS_COUNT; ++i) {
        sum += _bills[i] * BILL_VALUES[i];
    }
    return sum;
}

Cash& operator+=(Cash& c1, const Cash& c2) {
    for(unsigned int i(0); i < Cash::BILLS_COUNT; ++i) {
        c1.setBillCount(i, c1.getBillCount(i) + c2.getBillCount(i));
    }
    return c1;
}

Cash& operator-=(Cash& c1, const Cash& c2) {
    for(unsigned int i(0); i < Cash::BILLS_COUNT; ++i) {
        c1.setBillCount(i, c1.getBillCount(i) < c2.getBillCount(i)? 0: c1.getBillCount(i) - c2.getBillCount(i));
    }
    return c1;
}

QString toString(const Cash& cash) {
    if(!cash.sum())
        return QString("0");
    QString str;
    for(unsigned int i = 0; i < Cash::BILLS_COUNT; ++i) {
        if(cash.getBillCount(i)) {
            str = str + Cash::BILL_VALUES[i] + ": " + cash.getBillCount(i) + "\n";
         }
    }
    return str;
}





