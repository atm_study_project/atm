#include "Client.h"
#include <iostream>

Client::Client(size_t id, const QString& fn, const QString& ln, const QString& adr):
    _id(id), _first_name(fn), _last_name(ln), _adress(adr)
{
	return;
}

Client::Client(const Client& client):
    _id(client.id()), _first_name(client.firstName()),
    _last_name(client.lastName()), _adress(client.adress())
{
    return;
}

Client::~Client()
{
	return;
}

std::ostream& operator<<(std::ostream& os, const Client & client)
{
    os << client.id() << ' ' << client.firstName().toStdString();
    os << ' ' << client.lastName().toStdString() << ' ' << client.adress().toStdString();
	return os;
}
