#include "atm/atm.h"
#include "atm/card.h"
#include "atm/account.h"
#include "atm/transaction.h"
#include "atm/datasource.h"
#include "atm/receipt.h"
#include <iostream>
using namespace std;

#include <cassert>
#include <QDebug>
#include <QDateTime>
#include <qmath.h>


ATM::ATM():
    _dataSource(new DataSource()),
    _currentState(INITIAL)
{
    return;
}

ATM::~ATM()
{
    delete _dataSource;
    _dataSource = 0;
    return;
}

 bool ATM::isAuthorized() const
 {
     return _currentState == AUTHORIZED;
 }

 void ATM::switchToMaintenance()
 {
     _currentState = MAINTENANCE;
 }

 void ATM::switchToActive()
 {
     _currentState = INITIAL;
 }

Card* ATM::retrieveCard(unsigned long long cardNum) const
{
    Card * card(0);
    try {
        card = _dataSource->cardDao().read(cardNum);
    }
    catch(const DaoCard::MysqlException& ex) {
        qDebug() << ex.info();
    }
    return card;
}

Account* ATM::retrieveAccount(const Card& card) const
{
    Account * acc(0);
    try {
        acc = _dataSource->accountDao().read(card.account().id());
    }
    catch(const DaoAccount::MysqlException& ex) {
        qDebug() << ex.info();
    }
    return acc;
}

double ATM::round(double val) const
{
    return qFloor((val * 100.0) + 0.5) / 100.0;
}

double ATM::calcCommission(unsigned int amount, Account::AccountType type) const
{
    return round(amount * Account::COMMISSION[type]);
}

double ATM::commissionedAmount(unsigned int amount, Account::AccountType type) const
{
    return amount + calcCommission(amount, type);
}

void ATM::insertCard(const CreditCard* c) {
    if(_cardReader.hasCard()) {
        throw ATMException("Card already inserted!");
    }
    _cardReader.insertCard(c);
    _currentState = CARD_INSERTED;
    return;
}

const CreditCard* ATM::returnCard()
{
    _currentState = INITIAL;
    return _cardReader.returnCard();
}

void ATM::refillCash(const Cash& c) {
    if(_currentState != MAINTENANCE) {
        throw ATMException("ATM must be in maintenance mode");
    }
    _dispenser.refillCash(c);
}

unsigned int ATM::minimalBill() const {
    return _dispenser.minimalBill();
}

Cash ATM::currentCash() const
{
    return _dispenser.currentCash();
}

Receipt ATM::getReceipt() {
    return _printer.getReceipt();
}

bool ATM::hasReceipt() const
{
    return _printer.hasReceipt();
}

bool ATM::validCardAccount() const
{
   if(_cardReader.hasCard()) {
       return validCardAccount(_cardReader.read());
   }
   return false;
}

bool ATM::validCardAccount(unsigned long long cardNum) const
{
    bool res(false);
    Card * card = retrieveCard(cardNum);
    if(card) {
       Account * acc = retrieveAccount(*card);
       if(acc) {
           res = true;
           delete acc;
       }
       delete card;
    }
    return res;
}

bool ATM::balanceExceeds(unsigned int amount) const
{
    bool res(false);
    Card * card = retrieveCard(_cardReader.read());
    if(card) {
        Account * acc = retrieveAccount(*card);
        if(acc) {
            res = (acc->balance() >= commissionedAmount(amount, acc->type()));
            delete acc;
        }
        delete card;
     }
     return res;
}

bool ATM::canDispense(unsigned int amount) const
{
    return _dispenser.canDispense(amount);
}

bool ATM::checkPin(unsigned int pin)
{
    if(_currentState != CARD_INSERTED) {
        throw ATMException("Card has to be inserted for this operation");
    }
    Card * card = retrieveCard(_cardReader.read());
    if(!card) {
        throw ATMException("Unable to retrieve card information from database");
    }
    bool valid(pin == card->pin());
    if(valid) {
        _currentState = AUTHORIZED;
    }
    delete card;
    return valid;
}

double ATM::checkBalance()
{
    if(_currentState != AUTHORIZED) {
        throw ATMException("User has to be authorized for this operation");
    }
    Card * card = retrieveCard(_cardReader.read());
    if(!card) {
        throw ATMException("Unable to retrieve card information from database");
    }
    Account * acc = retrieveAccount(*card);
    delete card;
    if(!acc) {
        throw ATMException("Unable to retrieve account information from database");
    }
    double balance(acc->balance());
    delete acc;
    return balance;
}

Cash ATM::withdraw(unsigned int amount)
{
    if(_currentState != AUTHORIZED) {
        throw ATMException("User has to be authorized for this operation");
    }
    if (!_dispenser.canDispense(amount)) {
        throw ATMException("Cannot dispense stated amount");
    }
    Card * card = retrieveCard(_cardReader.read());
    if(!card) {
        throw ATMException("Unable to retrieve card information from database");
    }
    Account * acc = retrieveAccount(*card);
    if(!acc) {
        throw ATMException("Unable to retrieve account information from database");
    }

    double commission(calcCommission(amount, acc->type()));
    double fullAmount(amount + commission);
    if(acc->balance() < fullAmount) {
        throw ATMException("Requested withdrawal amount exceeds account balance");
    }
    try {

        acc->balance() -= fullAmount;
        _dataSource->accountDao().update(*acc);

        Transaction transaction(0, *card, QDateTime::currentDateTimeUtc().toTime_t(), -fullAmount, commission, "Cash withdrawal");
        _dataSource->transactionDao().create(transaction);

        Cash cash = _dispenser.dispense(amount);

        _printer.print(new Receipt(transaction.description(), card->number(),
            QDateTime::fromTime_t(transaction.date()), amount, transaction.commission()));

        delete card;
        delete acc;

        return cash;
    }
    catch(const DaoAccount::MysqlException& ex) {
        qDebug() << ex.info();
        throw ATMException("Unable to update account information in database");
    }
    catch(const DaoTransaction::MysqlException& ex) {
        qDebug() << ex.info();
        throw ATMException("Unable to save transaction information to database");
    }

}

void ATM::transferToAccount(unsigned long long cardNumber, unsigned int amount)
{
    if(_currentState != AUTHORIZED) {
        throw ATMException("User has to be authorized for this operation");
    }
    //Cannot find entered card number in database
    Card *cardTo = retrieveCard(cardNumber);
    if(!cardTo){
         throw ATMException("Card with entered number does not exist");
    }
    Account * accTo = retrieveAccount(*cardTo);
    if(!accTo) {
        throw ATMException("Unable to retrieve account information from database");
    }
    Card * cardFrom = retrieveCard(_cardReader.read());
    if(!cardFrom) {
        throw ATMException("Unable to retrieve card information from database");
    }
    Account * accFrom = retrieveAccount(*cardFrom);
    if(!accFrom) {
        throw ATMException("Unable to retrieve account information from database");
    }
    if(accFrom->id() == accTo->id()) {
        throw ATMException("Transfer sender and recipient should differ");
    }
    double commission(calcCommission(amount, accFrom->type()));
    double fullAmount(amount + commission);
    if(accFrom->balance() < fullAmount) {
        throw ATMException("Requested transfer amount exceeds account balance");
    }
    try {

        accFrom->balance() -= fullAmount;
        _dataSource->accountDao().update(*accFrom);
        accTo->balance() += amount;
        _dataSource->accountDao().update(*accTo);

        unsigned long long date(QDateTime::currentDateTimeUtc().toTime_t());
        Transaction transactionFrom(0, *cardFrom, date, -fullAmount, commission, "Transfer to another account");
        _dataSource->transactionDao().create(transactionFrom);
        Transaction transactionTo(0, *cardTo, date, amount, 0, "Transfer from another account");
        _dataSource->transactionDao().create(transactionTo);

        _printer.print(new Receipt(transactionFrom.description(), cardFrom->number(),
            QDateTime::fromTime_t(transactionFrom.date()), amount, transactionFrom.commission(),
            "Recipient: " + QString::number(cardTo->number())));

        delete cardFrom;
        delete cardTo;
        delete accFrom;
        delete accTo;

        return;
    }
    catch(const DaoCard::MysqlException& ex)
    {
        qDebug() << ex.info();
        throw ATMException("Unable to retrieve card information from database");
    }
    catch(const DaoAccount::MysqlException& ex)
    {
        qDebug() << ex.info();
        throw ATMException("Unable to retrieve/update account information from database");
    }
    catch(const DaoTransaction::MysqlException& ex)
    {
        qDebug() << ex.info();
        throw ATMException("Unable to save transaction information to database");
    }
}

void ATM::transferToMobile(QString number, unsigned int amount)
{
    if(_currentState != AUTHORIZED) {
        throw ATMException("User has to be authorized for this operation");
    }

    Card * card = retrieveCard(_cardReader.read());
    if(!card) {
        throw ATMException("Unable to retrieve card information from database");
    }
    Account * acc = retrieveAccount(*card);
    if(!acc) {
        throw ATMException("Unable to retrieve account information from database");
    }
    double commission(calcCommission(amount, acc->type()));
    double fullAmount(amount + commission);
    if(acc->balance() < fullAmount) {
        throw ATMException("Requested transfer amount exceeds account balance");
    }
    try {
        acc->balance() -= fullAmount;
        _dataSource->accountDao().update(*acc);

        Transaction transaction(0, *card, QDateTime::currentDateTimeUtc().toTime_t(), -fullAmount, commission, "Transfer to mobile number");
        _dataSource->transactionDao().create(transaction);


        _printer.print(new Receipt(transaction.description(), card->number(),
            QDateTime::fromTime_t(transaction.date()), amount, transaction.commission(),
            "Mobile number: " + number));

        delete card;
        delete acc;

        return;
    }
    catch(const DaoCard::MysqlException& ex)
    {
        qDebug() << ex.info();
        throw ATMException("Unable to retrieve card information from database");
    }
    catch(const DaoAccount::MysqlException& ex)
    {
        qDebug() << ex.info();
        throw ATMException("Unable to retrieve/update account information from database");
    }
    catch(const DaoTransaction::MysqlException& ex)
    {
        qDebug() << ex.info();
        throw ATMException("Unable to save transaction information to database");
    }
}

ATM::ATMException::ATMException(QString message):
    _message(message)
{
    return;
}

ATM::ATMException::ATMException(const ATM::ATMException& a):
    _message(a._message)
{
    return;
}

ATM::ATMException::~ATMException()
{
    return;
}

ATM::ATMException& ATM::ATMException::operator=(const ATM::ATMException& a) {
    _message = a._message;
    return (*this);
}

const QString& ATM::ATMException::getMessage() const {
    return _message;
}


