#include "Transaction.h"

Transaction::Transaction
(size_t id, const Card& card, unsigned long long date, double amount, double commision, const QString& description):
    _id(id), _card(card),
    _date(date), _amount(amount),
    _commission(commision),
    _description(description)
{
    return;
}

Transaction::Transaction(const Transaction& transaction):
    _id(transaction.id()), _card(transaction.card()),
    _date(transaction.date()), _amount(transaction.amount()),
    _commission(transaction.commission()),
    _description(transaction.description())
{
    return;
}

Transaction::~Transaction()
{
    return;
}

std::ostream& operator<<(std::ostream& os, const Transaction & transaction)
{
    os << transaction.id() << ' ' << transaction.card().number() << ' ';
    os << transaction.date() << ' ' << transaction.amount() << ' ';
    os << transaction.commission() << ' ' << transaction.description().toStdString();
    return os;
}
