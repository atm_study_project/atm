#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <QString>
#include <iostream>

// Entity class
class Client final
{
private:
	size_t _id;
    QString _first_name;
    QString _last_name;
    QString _adress;

public:

    static const QString tableName;
    static const QString idField;
    static const QString firstNameField;
    static const QString lastNameField;
    static const QString addressField;

    Client(
        size_t id=0,
        const QString& fn="",
        const QString& ln="",
        const QString& adr=""
		);
    Client(const Client&);
	~Client();

	inline const size_t & id() const {return _id;}
    inline const QString & firstName() const {return _first_name;}
    inline const QString & lastName() const {return _last_name;}
    inline const QString & adress() const {return _adress;}

	inline size_t & id() {return _id;}
    inline QString & firstName() {return _first_name;}
    inline QString & lastName() {return _last_name;}
    inline QString & adress() {return _adress;}
};

std::ostream& operator<<(std::ostream&, const Client &);

#endif
