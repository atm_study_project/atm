#include "printer.h"
#include "atm/atm.h"
#include "atm/receipt.h"

Printer::Printer()
{
    return;
}

Printer::~Printer()
{
    return;
}

void Printer::print(const Receipt* r)
{
    if(r == 0) {
        throw ATM::ATMException("Cannot print unitialized receipt");
    }
    _currentReceipt = r;
}

bool Printer::hasReceipt() const
{
    return _currentReceipt != 0;
}

Receipt Printer::getReceipt()
{
    Receipt r(*_currentReceipt);
    delete _currentReceipt;
    _currentReceipt = 0;
    return r;
}

