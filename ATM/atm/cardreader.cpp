#include "cardreader.h"
#include "atm.h"
#include "creditcard.h"

CardReader::CardReader():
    _currentCard(0)
{
    return;
}

CardReader::~CardReader()
{
    return;
}

void CardReader::insertCard(const CreditCard* c) {
    if(c == 0) {
        throw ATM::ATMException("Pointer to credit card cannot be uninitialized");
    }
    _currentCard = c;
    return;
}

bool CardReader::hasCard() const {
    return _currentCard != 0;
}

unsigned long long CardReader::read() const {
    return (!_currentCard)? 0: _currentCard->_number;
}

const CreditCard* CardReader::returnCard() {
    const CreditCard* c = _currentCard;
    _currentCard = 0;
    return c;
}
