#ifndef RECEIPT
#define RECEIPT

#include <QString>
#include <QDateTime>

struct Receipt
{
public:
    QString _title;
    unsigned long long _cardNumber;
    QDateTime _date;
    int _amount;
    double _commission;
    QString _description;

    Receipt(const QString& title,
            unsigned long long cardNumber,
            const QDateTime& date,
            int amount, double commission, const QString& = "");
    Receipt(const Receipt&);
    ~Receipt();
    Receipt& operator=(const Receipt&);

};

#endif // RECEIPT
