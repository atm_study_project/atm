#ifndef _DATA_SOURCE_
#define _DATA_SOURCE_

#include <QSharedPointer>

#include "atm.h"
#include "dao/accountdao.h"
#include "dao/carddao.h"
#include "dao/clientdao.h"
#include "dao/transactiondao.h"

class ATM::DataSource
{
public:
    DataSource(const QString& connName = "QMYSQL");
    ~DataSource();

    const DaoAccount& accountDao() const;
    const DaoCard& cardDao() const;
    const DaoClient& clientDao() const;
    const DaoTransaction& transactionDao() const;

private:
    const QString _connName;
    QSharedPointer<QSqlDatabase> _conn;

    mutable DaoAccount* _accountDao;
    mutable DaoCard* _cardDao;
    mutable DaoClient* _clientDao;
    mutable DaoTransaction* _transactionDao;
};

#endif // _DATA_SOURCE_

