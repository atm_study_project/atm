#ifndef _ACCOUNT_H_
#define _ACCOUNT_H_

#include <iostream>

#include "client.h"
#include <QString>

class Account final
{
public:

    static const QString tableName;
    static const QString idField;
    static const QString clientIdField;
    static const QString balanceField;
    static const QString typeField;

    enum AccountType {DEPOSIT=0, CREDIT, TYPES_COUNT};
    static double COMMISSION[TYPES_COUNT];

    Account(
            size_t id=0,
            const Client& c=Client(),
            double bal=0,
            const AccountType& type=AccountType(1)
            );
    Account(const Account&);
    ~Account();

    inline const size_t & id() const {return _id;}
    inline const Client & client() const {return _client;}
    inline const double & balance() const {return _balance;}
    inline const AccountType & type() const {return _type;}

    inline size_t & id() {return _id;}
    inline Client & client() {return _client;}
    inline double & balance() {return _balance;}
    inline AccountType & type() {return _type;}

private:
    size_t _id;
    Client _client;
    double _balance;
    AccountType _type;

};

std::ostream& operator<<(std::ostream&, const Account &);

#endif
